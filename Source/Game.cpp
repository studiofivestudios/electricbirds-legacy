#include <string>
#include <sstream>
#include <cstdio>
#include <windows.h>
#include <tlhelp32.h>


#include <Engine/Keys.h>
#include <Engine/Input.h>
#include <Engine/InputEvents.h>
#include <Engine/Sprite.h>
#include <Engine/Font.h>
#include <Engine/Gamepad.h>

#include "Game.h"

#include "..\Projects\AngryBirds\Mouse.h"
#include "..\Projects\AngryBirds\Scene.h"
#include "..\Projects\AngryBirds\Menu.h"
#include "..\Projects\AngryBirds\Level.h"
#include "..\Projects\AngryBirds\Physics.h"
#include "..\Projects\AngryBirds\Mathematics.h"
#include "..\Projects\AngryBirds\Audio.h"
//#include "..\Projects\AngryBirds\Profiles.h"

/**
*   @brief   Default Constructor.
*   @details Consider setting the game's width and height
             and even seeding the random number generator.
*/

AngryBirdsGame::AngryBirdsGame()
{
}

/**
*   @brief   Destructor.
*   @details Remove any non-managed memory and callbacks.
*/
AngryBirdsGame::~AngryBirdsGame()
{
	this->inputs->unregisterCallback(key_callback_id);
	this->inputs->unregisterCallback(mouse_callback_id);
}

/**
*   @brief   Initialises the game.
*   @details The game window is created and all assets required to
			 run the game are loaded. The keyHandler and clickHandler
			 callback should also be set in the initialise function.
*   @return  True if the game initialised correctly.
*/
bool AngryBirdsGame::init()
{
	setupResolution();
	if (!initAPI())
	{
		return false;
	}

	toggleFPS();
	this->setWindowTitle("Angry Birds - Initiate");


	font = new ASGE::Font();
	mouse = new Mouse();
	scene = new Scene();
	menu = new Menu();
	level = new Level();
	physics = new Physics();
	audio = new Audio();
	//profiles = new Profiles();

	if (!physics->initialize())
	{
		return false;
	}

	physics->delta_time = us.delta_time.count();

	menu_fade_text = new GameObject();
	game_loading_sprite_stage_one = new GameObject();
	game_loading_sprite_stage_two = new GameObject();
	game_loading_sprite_stage_three = new GameObject();
	game_loading_sprite_stage_four = new GameObject();
	game_loading_sprite_stage_five = new GameObject();
	game_menu_intro_text_fade_time = 0;

	chapters_text = new GameObject();
	fraps_warning_msg = new GameObject();

	if (!game_loading_sprite_stage_one->addSpriteComponent(renderer.get(), "Resources\\Misc\\loading-s1.png", scene) ||
		!game_loading_sprite_stage_two->addSpriteComponent(renderer.get(), "Resources\\Misc\\loading-s2.png", scene) ||
		!game_loading_sprite_stage_three->addSpriteComponent(renderer.get(), "Resources\\Misc\\loading-s3.png", scene) ||
		!game_loading_sprite_stage_four->addSpriteComponent(renderer.get(), "Resources\\Misc\\loading-s4.png", scene) ||
		!game_loading_sprite_stage_five->addSpriteComponent(renderer.get(), "Resources\\Misc\\loading-s5.png", scene))
	{
		return false;
	}

	//profiles->initialize(level);

	if (!menu_fade_text->addSpriteComponent(renderer.get(), "Resources\\Misc\\totally-not.png", scene))
	{
		return false;
	}

	if (!chapters_text->addSpriteComponent(renderer.get(), "Resources\\Misc\\chapters.png", scene))
	{
		return false;
	}

	chapters_text->spriteComponent()->getSprite()->xPos(623);
	chapters_text->spriteComponent()->getSprite()->yPos(30);

	menu_fade_text->spriteComponent()->getSprite()->xPos(581);
	menu_fade_text->spriteComponent()->getSprite()->yPos(762);
	//menu_fade_text->setOpacity(0.0f);

	/*
		Fraps causes ASGE to crash if it closes while the game is running, hence the user should be warned about this.
		Shows a warning message if it is detected.
	*/

	if (isFrapsRunning())
	{
		/*MessageBoxA(NULL, "Fraps has been known to crash OpenGL & D3D11 games on closing, please consider using an alternative application to capture your gameplay.
					\nIf you're looking for a free alternative, try Open Broadcast Software (https://obsproject.com/).","Warning", MB_OK);*/

		fraps_detected = true;
		if (!fraps_warning_msg->addSpriteComponent(renderer.get(), "Resources\\Misc\\fraps-warning.png", scene))
		{
			return false;
		}

		fraps_warning_msg->spriteComponent()->getSprite()->xPos(1072);
		fraps_warning_msg->spriteComponent()->getSprite()->yPos(301);
	}

	scene->setGameWidth(game_width);
	scene->setGameHeight(game_height);

	font_id = renderer->loadFont("Resources\\Fonts\\geonms-font.ttf", 24);

	// input handling functions
	/*
		Before enabling threading the game was slowing down to a halt, although the FPS is still dropping slightly.
	*/
	inputs->use_threads = true;

	key_callback_id = inputs->addCallbackFnc(
		ASGE::E_KEY, &AngryBirdsGame::keyHandler, this);
	
	mouse_callback_id =inputs->addCallbackFnc(
		ASGE::E_MOUSE_CLICK, &AngryBirdsGame::clickHandler, this);

	scroll_callback_id = inputs->addCallbackFnc(
		ASGE::E_MOUSE_SCROLL, &AngryBirdsGame::scrollHandler, this);

	mouse_move_callback_id = inputs->addCallbackFnc(ASGE::E_MOUSE_MOVE, &AngryBirdsGame::moveHandler, this);
	//controller_callback_id = inputs->addCallbackFnc(ASGE::E_GAMEPAD_STATUS, &AngryBirdsGame::controllerHandler, this);

	initial_load = true;

	return true;
}

/**
*   @brief   Sets the game window resolution
*   @details This function is designed to create the window size, any 
             aspect ratio scaling factors and safe zones to ensure the 
			 game frames when resolutions are changed in size.
*   @return  void
*/
void AngryBirdsGame::setupResolution()
{
	game_width = 1600;
	game_height = 900;
}

/**
*   @brief   Processes any key inputs
*   @details This function is added as a callback to handle the game's
			 keyboard input. For this game, calls to this function
			 are thread safe, so you may alter the game's state as you
			 see fit.
*   @param   data The event data relating to key input.
*   @see     KeyEvent
*   @return  void
*/
void AngryBirdsGame::keyHandler(const ASGE::SharedEventData data)
{
	auto key = static_cast<const ASGE::KeyEvent*>(data.get());
	
	if (key->key == ASGE::KEYS::KEY_ESCAPE && key->action == ASGE::KEYS::KEY_RELEASED)
	{
		if (!menu->getCurrentMenu() == menu->menus::MENU_INGAME)
		{
			signalExit();
		}
		else if (menu->getCurrentMenu() == menu->menus::MENU_INGAME)
		{
			menu->setCurrentMenu(menu->menus::MENU_PAUSED);
		}
		return;
	}

	/*if (key->key == ASGE::KEYS::KEY_ENTER && key->action == ASGE::KEYS::KEY_RELEASED)
	{
		if (menu->getCurrentMenu() == menu->menus::MENU_PAUSED || menu->getCurrentMenu() == menu->menus::MENU_MAINMENU)
		{
			menu->setCurrentMenu(menu->menus::MENU_INGAME);
		}
		return;
	}*/
}

/**
*   @brief   Processes any click inputs
*   @details This function is added as a callback to handle the game's
			 mouse button input. For this game, calls to this function
			 are thread safe, so you may alter the game's state as you
			 see fit.
*   @param   data The event data relating to key input.
*   @see     ClickEvent
*   @return  void
*/
void AngryBirdsGame::clickHandler(const ASGE::SharedEventData data)
{
	if (loading_complete)
	{
		auto click = static_cast<const ASGE::ClickEvent*>(data.get());

		//RMB
		if (click->button == 1)
		{
			mouse->setButton(mouse->buttons::RIGHT, click->action);
		}
		//LMB
		if (click->button == 0)
		{
			mouse->setButton(mouse->buttons::LEFT, click->action);

			//Mouse button has been clicked and released.
			if (click->action == 1)
			{
				bool is_in_main_menu = menu->getCurrentMenu() == menu->menus::MENU_MAINMENU;
				bool is_paused = menu->getCurrentMenu() == menu->menus::MENU_PAUSED;
				bool is_in_level_selection = menu->getCurrentMenu() == menu->menus::MENU_LEVELSELECTION;
				menu->processClick(mouse, scene, level, is_in_main_menu, is_paused, is_in_level_selection, audio);
			}
		}
		//MMB
		if (click->button == 2)
		{
			mouse->setButton(mouse->buttons::MIDDLE, click->action);
		}
	}
}

//Executes whenever the mouse is moved.
void AngryBirdsGame::moveHandler(const ASGE::SharedEventData data)
{
	if (loading_complete)
	{
		double x_pos, y_pos;
		inputs->getCursorPos(x_pos, y_pos);

		mouse->hasMoved(x_pos, y_pos, menu, us, scene, level, menu->getCurrentMenu() == menu->menus::MENU_LEVELSELECTION, menu->getCurrentMenu() == menu->menus::MENU_MAINMENU);
	}
}

void AngryBirdsGame::scrollHandler(const ASGE::SharedEventData data)
{
	if (loading_complete)
	{
		auto scroll = static_cast<const ASGE::ScrollEvent*>(data.get());
		float y_offset = scroll->yoffset;
		mouse->updateMouseScrolling(y_offset, us);
		return;
	}
}


/**
*   @brief   Updates the scene
*   @details Prepares the renderer subsystem before drawing the
			 current frame. Once the current frame is has finished
			 the buffers are swapped accordingly and the image shown.
*   @return  void
*/
void AngryBirdsGame::update(const ASGE::GameTime& us)
{
	switch (menu->getCurrentMenu())
	{
		case(Menu::menus::MENU_MAINMENU) :
		{
			this->setWindowTitle("Electric Birds - Menu");
			break;
		}
		case(Menu::menus::MENU_LEVELSELECTION):
		{
			this->setWindowTitle("Electric Birds - Level Selection");
			break;
		}
		case(Menu::menus::MENU_INGAME):
		{
			this->setWindowTitle("Electric Birds - In Game");
			break;
		}
		case(Menu::menus::MENU_PAUSED):
		{
			this->setWindowTitle("Electric Birds - Paused");
			break;
		}
		case(Menu::menus::MENU_PROFILES):
		{
			this->setWindowTitle("Electric Birds - Profiles");
			break;
		}
	}

	if (loading_complete)
	{
		if (menu->getCurrentMenu() == menu->menus::MENU_MAINMENU || menu->getCurrentMenu() == menu->menus::MENU_LEVELSELECTION)
		{
			audio->changeVolumeOfAllPriority(80.0f);
			if (audio->isFileInQueue("Resources\\Audio\\menu.ogg") == -1)
			{
				audio->queueAudio("Resources\\Audio\\menu.ogg", true, true, true);
			}
			if (audio->isFileInQueue("Resources\\Audio\\ingame_amb.ogg") >= 0)
			{
				audio->stopAudio(audio->isFileInQueue("Resources\\Audio\\ingame_amb.ogg"));
			}
		}
		else if (menu->getCurrentMenu() == menu->menus::MENU_INGAME)
		{
			audio->changeVolumeOfAllPriority(100.0f);
			if (audio->isFileInQueue("Resources\\Audio\\menu.ogg") >= 0)
			{
				audio->stopAudio(audio->isFileInQueue("Resources\\Audio\\menu.ogg"));
			}
			if (audio->isFileInQueue("Resources\\Audio\\ingame_amb.ogg") == -1)
			{
				audio->queueAudio("Resources\\Audio\\ingame_amb.ogg", true, true, true);
			}
		}
		else if (menu->getCurrentMenu() == menu->menus::MENU_LEVELOVER)
		{
			//When the game is paused, reduce the volume of all priority audio to 40% ("priority audio" = background music/sounds).
			audio->changeVolumeOfAllPriority(40.0f);
		}
		else if (menu->getCurrentMenu() == menu->menus::MENU_PAUSED)
		{
			level->setPaused(true);
			
			//When the game is paused, reduce the volume of all priority audio to 40% ("priority audio" = background music/sounds).
			audio->changeVolumeOfAllPriority(40.0f);
		}

		auto dt_sec = us.delta_time.count() / 1000.0;
		int interval = 25;
		if ((us.game_time.count() - time_since_last_check) >= interval)
		{
			mouse->updateDirection();
			time_since_last_check = us.game_time.count();
		}

		physics->delta_time = dt_sec;

		if (loading_complete_dt == 0)
		{
			loading_complete_dt = us.delta_time.count();
		}

		audio->onUpdate();

		/*
			The animation delta time is a modified delta time that accounts for the fact that the game had to load the rest of the game once the engine loaded first.
			It helps ensure that any animations are processed correctly.
		*/
		ani_dt = loading_complete_dt - us.delta_time.count();
		if (!menu_fade_text->spriteComponent()->getSprite()->opacity() < 100)
		{
			if (!game_loading_sprite_stage_five->spriteComponent()->getSprite()->opacity() <= 0.0f)
			{
				game_loading_sprite_stage_five->setOpacity(game_loading_sprite_stage_five->spriteComponent()->getSprite()->opacity() - (6.5 * (ani_dt / 1000)));
			}
		}

		if (menu->getCurrentMenu() == menu->menus::MENU_INGAME && !level->isGamePaused())
		{
			level->onUpdate(dt_sec);
		}

		for (int i = 0; i <= 32; i++)
		{
			if (menu->menu_storage[menu->getCurrentMenu()].objects[i].texture_file_location_1.length() < 1) 
			{
				break;
			}

			if (menu->menu_storage[menu->getCurrentMenu()].objects[i].game_object->attributes.visibility && menu->menu_storage[menu->getCurrentMenu()].objects[i].action != "pan")
			{

				if (mouse->current_cursor.spriteComponent()->getBoundingBox().isInside(menu->menu_storage[menu->getCurrentMenu()].objects[i].game_object->spriteComponent()->getBoundingBox()))
				{
					/*
						Set the mouse cursor to hover to reflect hovering over the menu item.
						We also disable the ability to pan the scene.
					*/

					if (menu->menu_storage[menu->getCurrentMenu()].objects[i].action != "background")
					{
						mouse->current_cursor = mouse->cursors[mouse->cursor_types::CURSOR_HOVER];
						scene->setCanPan(false);

						//We break here because otherwise it will check the other menu elements and set the cursor back to pointer.
						break;
					}
				}
				else
				{
					mouse->current_cursor = mouse->cursors[mouse->cursor_types::CURSOR_POINTER];
				}
			}
		
			if (menu->getCurrentMenu() == menu->menus::MENU_LEVELSELECTION)
			{
				for (int i = 0; i < level->num_of_levels; i++)
				{
					if (mouse->current_cursor.spriteComponent()->getBoundingBox().isInside(level->level_storage[i].tile->spriteComponent()->getBoundingBox()))
					{
						if (level->level_storage[i].tile->attributes.visibility)
						{
							mouse->current_cursor = mouse->cursors[mouse->cursor_types::CURSOR_HOVER];
							scene->setCanPan(false);
							break;
						}
					}
					else
					{
						mouse->current_cursor = mouse->cursors[mouse->cursor_types::CURSOR_POINTER];
						scene->setCanPan(false);
					}
				}
			}
		}
	}
	else
	{
		if (us.delta_time.count() > 1)
		{
			loadGame();
		}
	}
}

/**
*   @brief   Renders the scene
*   @details Renders all the game objects to the current frame.
	         Once the current frame is has finished the buffers are
			 swapped accordingly and the image shown.
*   @return  void
*/
void AngryBirdsGame::render(const ASGE::GameTime &)
{
	renderer->setFont(font_id);

	if (initial_load)
	{
		if (!game_loading_sprite_stage_five->spriteComponent()->getSprite()->opacity() <= 0.0f)
		{
			showLoadingSprite();
		}
		if (loading_complete)
		{
			for (int i = 0; i < scene->NUM_OF_BACKGROUNDS; i++)
			{
				renderer->renderSprite(*scene->background_sprite[i]);
			}

			Menu::menus current_menu = menu->getCurrentMenu();

			if (current_menu == menu->menus::MENU_MAINMENU)
			{
				for (int i = 0; i <= 32; i++)
				{
					if (menu->menu_storage[menu->MENU_MAINMENU].objects[i].texture_file_location_1.length() < 1)
					{
						break;
					}

					if (menu->menu_storage[menu->MENU_MAINMENU].objects[i].game_object->attributes.visibility)
					{
						renderer->renderSprite(*menu->menu_storage[menu->MENU_MAINMENU].objects[i].game_object->spriteComponent()->getSprite());
					}
				}
				renderer->renderSprite(*menu_fade_text->spriteComponent()->getSprite());

				if (fraps_warning_msg->spriteComponent() != nullptr)
				{
					renderer->renderSprite(*fraps_warning_msg->spriteComponent()->getSprite());
				}
			}
			else if (current_menu == menu->MENU_LEVELSELECTION)
			{
				/*
					This impacts on performance for some reason, maybe I'm simply asking too much of ASGE to render a parallax background
					and level selection screen at the same time?

					The FPS drops from ~700 down to ~500 (at one point with 60 level tiles loaded it was at ~100), seems to drop around 20fps
					per level tile (which is just a PNG with transparency with the level's name overlaid, so it should be easy for ASGE and
					the computer to render).

					I've looked into the problem and it seems that this is bottlenecking the render
					thread (which isn't async for obvious reasons). It was dropping down to exactly 24 fps (with no spikes in CPU or RAM usage)
					when I was also creating variables within this function too (not inside the loop, mind), so something to do with this effectively
					bottlenecking the entire render thread (delta time does not seem to be affected, but that's why it exists...).
				*/

				for (int i = 0; i <= 32; i++)
				{
					if (menu->menu_storage[menu->MENU_LEVELSELECTION].objects[i].texture_file_location_1.length() < 1)
					{
						break;
					}
					renderer->renderSprite(*menu->menu_storage[menu->MENU_LEVELSELECTION].objects[i].game_object->spriteComponent()->getSprite());
				}

				if (ls_curr_world.length() < 1)
				{
					ls_curr_world = "01";
				}

				std::ostringstream world_information;
				std::string chapter_id = level->getChapterIdAsString(&level->chapter_name_closest_to_top);
				int selected_world_num = level->getChapterId(&level->chapter_name_closest_to_top);
				if (level->getNumOfLevelsInWorld(selected_world_num) < 1)
				{
					world_information << "CHAPTER " << level->getChapterIdAsString(&level->chapter_name_closest_to_top) << " | "
						<< "NO LEVELS FOUND";
				}
				else
				{
					world_information << "CHAPTER " << level->getChapterIdAsString(&level->chapter_name_closest_to_top) << " | "
						<< level->getNumOfCompletedLevelsForWorld(selected_world_num) << " / "
						<< level->getNumOfLevelsInWorld(selected_world_num) << " LEVELS COMPLETED";
				}

				for (int i = 0; i < level->CHAPTER_COUNT; i++)
				{
					if (level->getChapterName(i)->attributes.visibility)
					{
						renderer->renderSprite(*level->getChapterName(i)->spriteComponent()->getSprite());
					}
				}

				renderer->renderText(world_information.str(), 50, 125, 1, ASGE::COLOURS::WHITE);
				for (int i = 0; i < level->num_of_levels; i++)
				{
					level_x_pos = level->level_storage[i].tile->spriteComponent()->getSprite()->xPos();
					level_y_pos = level->level_storage[i].tile->spriteComponent()->getSprite()->yPos();
					level_tile_height = level->level_storage[i].tile->spriteComponent()->getSprite()->height();
					if (level->level_storage[i].tile->attributes.visibility)
					{ 
						renderer->renderSprite(*level->level_storage[i].tile->spriteComponent()->getSprite());
						if (level->level_storage[i].is_locked)
						{
							renderer->renderText("LOCKED", level_x_pos + level->level_storage[i].tile->spriteComponent()->getSprite()->width() / 2, level_y_pos + (level_tile_height / 2), 1,
								ASGE::COLOURS::WHITE);
						}
						else
						{
							renderer->renderText(level->level_storage[i].name, level_x_pos + 65, level_y_pos + (level_tile_height / 2), 1, ASGE::COLOURS::WHITE);

							if (level->level_storage[i].is_completed)
							{
								level->level_storage[i].completed_icon->spriteComponent()->getSprite()->xPos(level->level_storage[i].tile->spriteComponent()->getSprite()->xPos() +
									level->level_storage[i].tile->spriteComponent()->getSprite()->width() - 30);
								level->level_storage[i].completed_icon->spriteComponent()->getSprite()->yPos(level->level_storage[i].tile->spriteComponent()->getSprite()->yPos() - 20);
								renderer->renderSprite(*level->level_storage[i].completed_icon->spriteComponent()->getSprite());
							}
						}
					}
				}
			}
			else if (current_menu == menu->menus::MENU_PAUSED)
			{
				level->renderLevel();

				for (int i = 0; i <= 32; i++)
				{
					if (menu->menu_storage[menu->MENU_PAUSED].objects[i].texture_file_location_1.length() < 1) 
					{
						break;
					}
					renderer->renderSprite(*menu->menu_storage[menu->MENU_PAUSED].objects[i].game_object->spriteComponent()->getSprite());
					std::ostringstream score_s;
					score_s << "SCORE : " << level->getCurrentScore();
					renderer->renderText(score_s.str().c_str(), 1400, 30, 1, ASGE::COLOURS::WHITE);
				}

				renderer->renderText(level->getCurrentLevel()->name, 188, 59, 1, ASGE::COLOURS::WHITE);
			}
			else if (current_menu == menu->menus::MENU_LEVELOVER)
			{
				scene->setCanPan(false);

				for (int i = 0; i <= 32; i++)
				{
					if (menu->menu_storage[menu->MENU_LEVELOVER].objects[i].texture_file_location_1.length() < 1)
					{
						break;
					}

					// Do not show the "next level" button if the player is playing the last level.

					if (!level->isCurrentLevelLast() && menu->menu_storage[menu->MENU_LEVELOVER].objects[i].action == "next-level")
					{
						renderer->renderSprite(*menu->menu_storage[menu->MENU_LEVELOVER].objects[i].game_object->spriteComponent()->getSprite());
					}
					else if(menu->menu_storage[menu->MENU_LEVELOVER].objects[i].action != "next-level")
					{
						renderer->renderSprite(*menu->menu_storage[menu->MENU_LEVELOVER].objects[i].game_object->spriteComponent()->getSprite());
					}
					else
					{
						// Required to disable clicks on the button (because although it's not being rendered, it's still there).
						menu->menu_storage[menu->MENU_LEVELOVER].objects[i].game_object->attributes.visibility = false;
					}
				}

				bool has_won = level->getCurrentLevel()->is_completed_this_round;
				
				if (has_won)
				{
					renderer->renderText("LEVEL CLEARED!", 725, 320, 1.0, ASGE::COLOURS::WHITE);
				}
				else
				{
					renderer->renderSprite(*level->level_failed->spriteComponent()->getSprite());
				}

				std::ostringstream score_txt;
				score_txt << "SCORE: " << level->getCurrentScore();
				renderer->renderText(score_txt.str(), 743, 450, 1.0, ASGE::COLOURS::WHITE);
			}
			else if (current_menu == menu->menus::MENU_HOW_TO_PLAY)
			{
				for (int i = 0; i <= 32; i++)
				{
					if (menu->menu_storage[menu->MENU_HOW_TO_PLAY].objects[i].texture_file_location_1.length() < 1)
					{
						break;
					}
					renderer->renderSprite(*menu->menu_storage[menu->MENU_HOW_TO_PLAY].objects[i].game_object->spriteComponent()->getSprite());
				}
			}
			else if (current_menu == menu->menus::MENU_PROFILES)
			{
				/*for (int i = 0; i <= 32; i++)
				{
					if (menu->menu_storage[menu->MENU_PROFILES].objects[i].texture_file_location_1.length() < 1)
					{
						break;
					}

					if (menu->menu_storage[menu->MENU_PROFILES].objects[i].action == "main-menu" && profiles->getAmountOfLoadedProfiles() < 1)
					{

					}
					else if (menu->menu_storage[menu->MENU_PROFILES].objects[i].action == "no-profiles-detected-message" && profiles->getAmountOfLoadedProfiles() < 1)
					{
						renderer->renderSprite(*menu->menu_storage[menu->MENU_PROFILES].objects[i].game_object->spriteComponent()->getSprite());
					}
					else
					{
						renderer->renderSprite(*menu->menu_storage[menu->MENU_PROFILES].objects[i].game_object->spriteComponent()->getSprite());
					}
				}

				renderer->renderText("USER PROFILES", 50, 50, 1, ASGE::COLOURS::WHITE);
				renderer->renderText("SELECT A PROFILE TO USE,\nOR CREATE A NEW ONE", 50, 90, 1, ASGE::COLOURS::WHITE);*/
			}
			else
			{
				level->renderLevel();

				std::ostringstream score_s;
				score_s << "SCORE : " << level->getCurrentScore();
				renderer->renderText(score_s.str().c_str(), 1400, 30, 1, ASGE::COLOURS::WHITE);
			}

			renderer->renderSprite(*mouse->current_cursor.spriteComponent()->getSprite());

			if (diag_mode)
			{
				double font_size = 6;
				int number_of_lines = 3;
				std::string controller_state = "";
				std::ostringstream diag_first_line;
				diag_first_line << "Mouse Position X: " << mouse->getXPos() << " | Mouse Position Y: " << mouse->getYPos() << " | Game Left X: " << scene->game_left_x << " | Game Right X: " << scene->game_right_x << "\nLMB: " << mouse->lmb_status << " | RMB: " << mouse->rmb_status << "\nMaximum Panning Width: " << scene->maximum_pan_width << " pixels | Can Pan: " << scene->canPan() << "\nMenus Loaded: " << menu->num_of_menus;

				diag_first_line << " | Cannon Character Velocity: " << level->getCannonCharacterVelocity();

				renderer->renderText(diag_first_line.str().c_str(), game_width - 400 - (font_size * diag_first_line.str().length() / number_of_lines), 25, 1, ASGE::COLOURS::WHITE);
			}
		}
	}
}

/*	Sets the window title.
	This was supposed to be in a separate class but couldn't get the renderer to work from the other class.

	Returns bool in case we want to do something with the result later.
*/
bool AngryBirdsGame::setWindowTitle(std::string desired_window_title)
{
	//If length is less than 4 characters then don't change the title (it's too short!).
	if (desired_window_title.length() < 4)
	{
		return false;
	}
	else
	{
		std::ostringstream build_info;
		build_info << desired_window_title << " - " << getRenderLibrary() <<" - " << __DATE__ << " (" << __TIME__ << ")";
		renderer->setWindowTitle(build_info.str().c_str());
		return true;
	}
}

std::string AngryBirdsGame::getRenderLibrary()
{
	switch (renderer->getRenderLibrary())
	{
		case (ASGE::Renderer::RenderLib::GLEW):
		{
			return "OpenGL (Extension Wrangler)";
		}
		case (ASGE::Renderer::RenderLib::PDCURSES):
		{
			return "Public Domain Curses (x64)";
		}
		case (ASGE::Renderer::RenderLib::INVALID):
		{
			return "Public Domain Curses (x32)";
		}
		default:
		{
			return "UNKNOWN RENDERER";
		}
	}
}

/*
	Centre a given string, (credit) from a Stack Overflow question I posted back in 2013:
	https://stackoverflow.com/q/17512825/2422013
*/
std::string AngryBirdsGame::centreString(std::string input, int width)
{
	return std::string((renderer->getActiveFont().font_size % width + 5 - input.length()) / 2, ' ') + input;
}

void AngryBirdsGame::loadGame() {
	//We must break out of this loop when a class has loaded since we need to update the loading screen.
	if (!level->hasLoaded())
	{
		if (level->initialize(&renderer, *scene, *mouse, *physics, *menu, *audio))
		{
			loading_stage++;
			return;
		}
	}
	if (!scene->hasLoaded() && !audio->hasLoaded())
	{
		if (scene->initialize(&renderer))
		{
			loading_stage++;
			return;
		}
	}
	if (!menu->hasLoaded())
	{
		if (menu->initialize(&renderer, scene, level))
		{
			loading_stage++;
			return;
		}
	}
	if (!mouse->hasLoaded())
	{
		if (mouse->initialize(&renderer, &inputs, scene, menu))
		{
			audio->initialize(scene);
			loading_stage++;
			loading_complete = true;
			return;
		}
	}
	
}

void AngryBirdsGame::showLoadingSprite()
{
	if (loading_stage == 1)
	{
		renderer->renderSprite(*game_loading_sprite_stage_one->spriteComponent()->getSprite());
		return;
	}
	else if (loading_stage == 2)
	{
		renderer->renderSprite(*game_loading_sprite_stage_two->spriteComponent()->getSprite());
		return;
	}
	else if (loading_stage == 3)
	{
		renderer->renderSprite(*game_loading_sprite_stage_three->spriteComponent()->getSprite());
		return;
	}
	else if (loading_stage == 4)
	{
		renderer->renderSprite(*game_loading_sprite_stage_four->spriteComponent()->getSprite());
		return;
	}
	else if (loading_stage == 5)
	{
		renderer->renderSprite(*game_loading_sprite_stage_five->spriteComponent()->getSprite());

		/*if (profiles->getAmountOfLoadedProfiles() < 1)
		{
		menu->setCurrentMenu(menu->MENU_PROFILES);
		scene->can_move_on_x = false;
		}
		else*/
		//{
		menu->setCurrentMenu(menu->MENU_MAINMENU);
		//}

		// Loading stage of -1 means that loading is complete.
		loading_stage = -1;
	}
}


/*
	Fraps crashes ASGE if it unhooks from ASGE (usually when closing), so we need to detect if Fraps is running
	and then warn the player if it is.

	This is not an issue unique to ASGE (Unreal Engine has the same issue of Fraps causing random crashes and shows a message
	to the user when starting up and if Fraps is detected).

	Code adapted from https://stackoverflow.com/a/14751302/2422013.
*/
bool AngryBirdsGame::isFrapsRunning()
{
	PROCESSENTRY32 entry;
	entry.dwSize = sizeof(PROCESSENTRY32);

	HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);

	wchar_t process_name[] = L"fraps.exe";

	if (Process32First(snapshot, &entry))
	{
		while (Process32Next(snapshot, &entry))
		{
			if (!lstrcmpW(entry.szExeFile, process_name))
			{
				CloseHandle(snapshot);
				return true;
			}
		}
	}

	CloseHandle(snapshot);
	return false;
}