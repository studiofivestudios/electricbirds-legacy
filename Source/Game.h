#pragma once
#include <string>
#include <thread>
#include <Engine/OGLGame.h>

#include "GameObject.h"
#include "Rect.h"

//Forward declarations to stop cyclic reference errors.
class Mouse;
class Level;
class Scene;
class Menu;
class Level;
class Physics;
class Audio;
class Controller;
class Profiles;

/**
*  An OpenGL Game based on ASGE.
*/
class AngryBirdsGame :
	public ASGE::OGLGame
{
public:
	AngryBirdsGame();
	~AngryBirdsGame();
	virtual bool init() override;
	bool isFrapsRunning();																				// Is Fraps running?

	bool diag_mode = false;																				// Enable or disable the diagnostic messages shown on the upper right of the screen.

private:
	void keyHandler(const ASGE::SharedEventData data);													// The key handler function (handles key presses).
	void clickHandler(const ASGE::SharedEventData data);												// Handles mouse clicks.
	void moveHandler(const ASGE::SharedEventData data);													// Handles mouse movement.
	void scrollHandler(const ASGE::SharedEventData data);												// Handles mouse scroll.
	void setupResolution();																				// Sets up game resolution.
	void loadGame();																					// Load all core game functions, executed once the engine has loaded.
	void showLoadingSprite();																			// Shows the game loading screen.

	virtual void update(const ASGE::GameTime &) override;												// Core game update function, executed on every tick.
	virtual void render(const ASGE::GameTime &) override;												// Core game render function, called after the game update function.
	bool setWindowTitle(std::string desired_window_title);												// Sets the title of the window.
	std::string centreString(std::string input, int width);												// Pad a string with spaces to fill a defined width.
	std::string getRenderLibrary();																		// Returns the name of the render library used.

	int  key_callback_id = -1;																			// Keyboard key callback ID. Used for event handling.
	int  mouse_callback_id = -1;																		// Mouse button callback ID. Used for event handling.
	int  mouse_move_callback_id = -1;																	// Mouse move callback ID. Used for event handling.
	int  scroll_callback_id = -1;																		// Mouse scroll callback ID. Used for event handling.

	int score;																							// The game score.
	int font_id;																						// The current font ID that is being used.

	Mouse *mouse = nullptr;																				// Pointer to the Mouse class.
	Scene *scene = nullptr;																				// Pointer to the Scene class.
	Menu *menu = nullptr;																				// Pointer to the Menu class.
	Level *level = nullptr;																				// Pointer to the Level class.
	Physics *physics = nullptr;																			// Pointer to the Physics class.
	ASGE::Font *font = nullptr;																			// Pointer to the ASGE Font class.
	Audio *audio = nullptr;																				// Pointer to the Audio class.
	Profiles *profiles = nullptr;																		// Pointer to the profile system.

	GameObject *menu_fade_text = nullptr;																// Pointer for the game name on the menu title screen.
	GameObject *game_loading_sprite_stage_one = nullptr;												// Pointer for the stage 1 loader image.
	GameObject *game_loading_sprite_stage_two = nullptr;												// Pointer for the stage 2 loader image.
	GameObject *game_loading_sprite_stage_three = nullptr;												// Pointer for the stage 3 loader image.
	GameObject *game_loading_sprite_stage_four = nullptr;												// Pointer for the stage 4 loader image.
	GameObject *game_loading_sprite_stage_five = nullptr;												// Pointer for the stage 5 loader image.
	GameObject *chapters_text = nullptr;																// Text containing the word "CHAPTERS".
	GameObject *fraps_warning_msg = nullptr;

	double time_since_last_check;																		// Time since last check, used for the menu title screen game name.
	double ani_dt;																						// The animation delta time. Starts from when the game has fully loaded.
	double loading_complete_dt;																			// Aids in calculating the animation delta time.

	bool initial_load = false;																			// Stores if the game engine has fully loaded.
	bool fraps_detected = false;

	double level_x_pos;																					// Stores the current panning x position of the level.
	double level_y_pos;																					// Stores the current panning y position of the level.
	double level_tile_height;																			// Stores the height of level tiles.

	int loading_stage = 1;																				// Stores the current loading stage, used for rendering the correct loading stage screen.
	std::string ls_curr_world;																			// Stores the current level selection world.

	double game_menu_intro_text_fade_time;																// Stores the current fade-in state of the menu title screen's game name.
	double game_menu_intro_fade_in_interval = 1000;														// The duration of the fade-in animation for the menu title screen's game name.
	double game_menu_buttons_fade_in_interval = 500;													// As above, but for the menu's buttons.
	double game_menu_buttons_opacity;																	// Stores the current fade-in opacity of the game menu buttons.
	bool has_started_object_load = false;																// Has the game started loading?
	bool loading_complete = false;																		// Has the game finished loading?
};