/*
	This class is responsible for all GameObjects.

	Core ASGE component.
*/

#pragma once
#include <string>
#include "SpriteComponent.h"
#include "Vector2.h"

#include "..\Projects\AngryBirds\Physics.h"
#include "..\Projects\AngryBirds\Audio.h"

class GameObject
{
public:
	GameObject() = default;
	~GameObject();

	bool addSpriteComponent(ASGE::Renderer* renderer, const std::string& texture_file_name, Scene *scene);		// Allocates and attaches a sprite to this GameObject.
	SpriteComponent* spriteComponent();																			// Public accessiblility for sprite_component.
	bool resetSprite(ASGE::Renderer* renderer);																	// Frees the sprite and re-creates it.
	std::string getTextureFile();
	void setOpacity(float percent);																				// Sets opacity but has a upper and lower limit of 0 and 100 respectively.

	Audio *audioComponent = nullptr;																			// Per-sprite audio system.

	struct attrs 
	{
		vector2 direction = { 0,0 };																			// Stores the direction the object is moving.
		bool visibility;																						// Is the object visible?
		Mathematics::pos bezier_positions[Physics::BEZIER_RESOLUTION];											// An array of positions for Bezier curves.
		int current_bezier_position;																			// The current Bezier curve position, if applicable.
		bool bezier_animation_done;																				// Is the Bezier curve animation complete?
		bool bezier_positions_loaded;																			// Have the Bezier positions been calculated and are ready to use?
		bool ready_to_fire;																						// Can we fire the object?
		bool is_spent = false;																					// Do we no longer need the object?
		bool can_roll = false;																					// Can the object roll?
		bool has_fired = false;																					// Has the object been fired from the cannon?
		double spent_timer;																						// The time (in ms) that the object has spent in a spent state.
		Physics::physical_body physical_params;																	// Physical parameters of the object (weight, etc.)
	};

	attrs attributes;																							// A container to store the attrs struct.

private:
	void freeSpriteComponent();																					// Destroy the sprite and associated component.
	SpriteComponent* sprite_component = nullptr;																// Pointer to the sprite component.
	Scene *scene;																								// Pointer to the scene class;
	void init(Scene *scene);																					// Inits the GameObject.
	std::string file;																							// Stores the file path to the file used as the sprite's image.
};