/*
	This class is responsible for all physics calculations

	Created : 16th Febuary 2018 @ 18:43
	Author : Alexander Stopher
*/

#include "Physics.h"

#include <GameObject.h>

Physics::Physics()
{
}


Physics::~Physics()
{
}

bool Physics::initialize()
{
	loaded = true;
	return true;
}

void Physics::calculateBezierTrajectory(GameObject *sprite, Mathematics::pos mouse_pos, Mathematics::pos floor_pos, Mathematics::pos a, double game_height, double game_width)
{
	/*
		sprite	:	The sprite that the trajectory applies to.
		a		:	The object's start point
		b		:	The object's end point.

		CONTROL:
			If cursor moves away from the slingshot		:	Make curve longer (move control point further along X)
			If cursor moves downward					:	Make curve higher (move control point nearer 0 on Y)
			If cursor moves upward						:	Make curve lower  (move control point further along X)
	*/

	Mathematics::pos control_point;
	Mathematics::pos b;
	Mathematics::pos sprite_pos;
	double y_modifier;

	control_point.x = (Mathematics::getDistance(mouse_pos.x, sprite->spriteComponent()->getSprite()->xPos()) * 5.5) * -1;
	control_point.y = (game_height - mouse_pos.y) / 0.3;
	
	//double modifier = sprite->attributes.physical_params.weight / 10;
	
	b = calculateBezierEndPoint(control_point, a, floor_pos.y, mouse_pos);

	for (int t = 0; t < (double)BEZIER_RESOLUTION; t++)
	{
		double resolution_simplified = (double)t / (double)BEZIER_RESOLUTION;
		sprite->attributes.bezier_positions[t].x = (1 - resolution_simplified) * (1 - resolution_simplified) * (sprite->spriteComponent()->getSprite()->xPos() + sprite->spriteComponent()->getSprite()->width() / 2) + 2 * (1 - resolution_simplified) * resolution_simplified * control_point.x + resolution_simplified * resolution_simplified * b.x;
		sprite->attributes.bezier_positions[t].y = (1 - resolution_simplified) * (1 - resolution_simplified) * sprite->spriteComponent()->getSprite()->yPos() + 2 * (1 - resolution_simplified) * resolution_simplified * control_point.y + resolution_simplified * resolution_simplified * b.y;
	}
}

void Physics::calculateBezierTrajectory(GameObject *sprite, Mathematics::pos control_point, Mathematics::pos a, Mathematics::pos b, double resolution)
{
	for (int t = 0; t < (double)resolution; t++)
	{
		double resolution_simplified = (double)t / (double)resolution;
		sprite->attributes.bezier_positions[t].x = (1 - resolution_simplified) * (1 - resolution_simplified) * (sprite->spriteComponent()->getSprite()->xPos() + sprite->spriteComponent()->getSprite()->width() / 2) + 2 * (1 - resolution_simplified) * resolution_simplified * control_point.x + resolution_simplified * resolution_simplified * b.x;
		sprite->attributes.bezier_positions[t].y = (1 - resolution_simplified) * (1 - resolution_simplified) * sprite->spriteComponent()->getSprite()->yPos() + 2 * (1 - resolution_simplified) * resolution_simplified * control_point.y + resolution_simplified * resolution_simplified * b.y;
	}
}

Mathematics::pos Physics::calculateBezierEndPoint(Mathematics::pos control_point, Mathematics::pos reference_pos, double stage_floor_offset, Mathematics::pos mouse_pos)
{
	Mathematics::pos b;
	//b.x = (Mathematics::getDistance(mouse_pos, reference_pos)) * 3;
	b.x = (control_point.x) * 3;
	b.y = stage_floor_offset;
	return b;
}

/*
	Calculate gravitational forces and return an set of x & y coordinates.
	Takes into account the sprite's stored weight to add realism.
	Tested to be far smoother than the basic Bezier curve that was implemented previously, when compared to the current
		gravity-assisted Bezier curve:
			Old curve: https://youtu.be/fCYhYZlVcBQ
			New gravity-assisted curve: https://youtu.be/iAPgLEYlJBY

	Adapted algorithm from https://gamedev.stackexchange.com/a/41917/51403 (with enhancements).
*/
Mathematics::pos Physics::calculateGravitySpeedInterpolation(GameObject &sprite, bool is_moving_upwards, float pos_x, float pos_y, int vec_x, int vec_y)
{
	auto dt_time = delta_time / 1000.0;
	Mathematics::pos calculated_pos;
	double gravity = 9.81;
	double weight = (double)sprite.attributes.physical_params.weight;
	double force = 0;

	//Default to 2kg if there is no weight (otherwise the force is infinite).
	if (weight < 1)
	{
		force = gravity / 2;
	}
	else
	{
		force = gravity / weight;
	}

	calculated_pos.x = pos_x;
	calculated_pos.y = pos_y;

	sprite.attributes.physical_params.time_stepping += dt_time;

	double calc = ((sprite.attributes.direction.speed * force) * 2) * dt_time;

	if (vec_x == 1)
	{
		calculated_pos.x += calc;
		sprite.attributes.direction.speed += delta_time * gravity;
	}
	else if (vec_x == -1)
	{
		calculated_pos.x -= calc;
		sprite.attributes.direction.speed -= delta_time * gravity;
	}

	if (vec_y == 1)
	{
		calculated_pos.y += calc;
	}
	else if (vec_y == -1)
	{
		calculated_pos.y -= calc;
	}

	return calculated_pos;
}

float Physics::calculateRotationsPerUpdate(GameObject &sprite)
{	
	return Mathematics::degToRad(3*(sprite.attributes.direction.speed) / (sprite.spriteComponent()->getSprite()->width() / 2) * (delta_time * (3 * (sprite.attributes.direction.speed))));
}

bool Physics::hasLoaded()
{
	return loaded;
}