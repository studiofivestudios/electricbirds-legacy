/*
	This class is responsible for all mouse input and interpretation, such as clicks.

	Created: 13th Febuary 2018 @ 23:34
	Author: Alexander Stopher
*/

#pragma once

#include <Engine/Input.h>
#include <Engine/InputEvents.h>
#include <Engine/Sprite.h>
#include <Engine/Mouse.h>
#include <Engine/Gametime.h>

#include <GameObject.h>

#include "Scene.h"
#include "Menu.h"

class Mouse
{
public:
	Mouse();
	~Mouse();

	static const int CURSORS_AMOUNT = 3;
	static const int BUTTONS_AMOUNT = 3;

	enum buttons { LEFT = 0, RIGHT = 1, MIDDLE = 2 };
	enum actions { PRESSED = 0, RELEASED = 1 };
	enum direction { MOVE_LEFT = 0, MOVE_RIGHT = 1, MOVE_STATIONARY = -1};
	enum cursor_state { CURSOR_SHOW = 0, CURSOR_HIDE = 1 };
	enum cursor_types { CURSOR_POINTER = 0, CURSOR_HOVER = 1, CURSOR_GRAB = 2 };

	bool initialize(std::unique_ptr<ASGE::Renderer> *renderer, std::unique_ptr<ASGE::Input> *input, Scene *scene, Menu *menu); //Initialize the mouse.

	void setButton(buttons button, bool released); //True being released, false if it's being held down.
	void hasMoved(double mouse_x, double mouse_y, Menu *menu, const ASGE::GameTime& us, Scene *scene, Level *level, bool in_level_selection, bool in_game);
	void updateMouseScrolling(float y_offset, const ASGE::GameTime& us);
	double getXPos();
	double getYPos();
	void updateDirection();

	ASGE::Sprite *getSprite(int num);
	GameObject *getGameObject(int num);
	void setCursorType(cursor_types type); //Sets which cursor is currently visible.
	bool hasLoaded();

	GameObject cursors[CURSORS_AMOUNT] = {}; //0 = pointer, 1 = hand, 2 = grab
	ASGE::Sprite *cursor_sprite[CURSORS_AMOUNT] = {};
	int button_is_not_released[BUTTONS_AMOUNT] = {};

	const char *lmb_status;
	const char *rmb_status;
	double last_x_pos;
	int mouse_dir;
	GameObject current_cursor;

private:
	double x_pos;
	double y_pos;
	bool loaded = false;
	bool hasStartedInit = false;

	Menu *menu = nullptr;

	void action(buttons button); //The primary input handler for the mouse.
	void setCursorStatus(ASGE::CursorMode cursor);

	std::unique_ptr<ASGE::Renderer> *renderer = nullptr;
	std::unique_ptr<ASGE::Input>	*input   = nullptr;
};

