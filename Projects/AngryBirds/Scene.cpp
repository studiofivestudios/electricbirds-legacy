/*
	This class is responsible for all scene actions, such as loading new levels and moving the level camera.

	Created: 15th Febuary 2018 @ 12:37
	Author: Alexander Stopher
*/

#include <Game.h>
#include <sstream>

#include "Scene.h"
#include "Level.h"

Scene::Scene()
{
}


Scene::~Scene()
{
}


bool Scene::initialize(std::unique_ptr<ASGE::Renderer> *renderer)
{
	if (!hasStartedInit)
	{
		hasStartedInit = true;
		is_sound_muted = false;
	
		this->renderer = renderer;

		std::ostringstream file_creator;

		std::srand(time(NULL));
		int background_rnd = std::rand() % 3;
		for (int i = 0; i <= NUM_OF_BACKGROUNDS-1; i++)
		{
			file_creator << "Resources\\Backgrounds\\parallax-" << background_rnd << "\\" << i + 1 << ".png";
			std::string texture = file_creator.str();
			parallax_background[i].texture = texture.c_str();
			parallax_background[i].pan_speed = i*0.6;

			file_creator.str("");

			if (!background[i].addSpriteComponent(renderer->get(), parallax_background[i].texture, this))
			{
				return false;
			}

			background_sprite[i] = background[i].spriteComponent()->getSprite();
		}

		/*NOTE: On creation of menus this should be false as we only want the background to move in-game.*/
		can_move_on_x = false;

		/* Set the maximum pan width (with the minimum being 0).*/
		maximum_pan_width = background_sprite[NUM_OF_BACKGROUNDS-1]->width();

		loaded = true;
	}

	return true;
}

void Scene::panGameScene(bool right, Menu *menu, const ASGE::GameTime& us, Level *level, bool &in_level_selection, bool &in_menu)
{
	/* 
		Later on when we add objects we want them to pan too.
		Issues:
			- Should really animate with delta time however this causes animation errors (with the "pan" object moving at different rates, causing it to be mis-placed).
			  Or should we? This is a fixed-time animation which is animated by mouse movement, so maybe shouldn't be using delta time (as it's not automated, the player controls it).
			- If player pans really slowly then their movement is interpreted as the opposite direction.
	*/

	double x_pos_right_boundary = 1515;

	if (menu->getCurrentMenu() == menu->MENU_INGAME && can_move_on_x)
	{
		for (int i = 0; i < NUM_OF_BACKGROUNDS; i++)
		{
			if (game_left_x > 0 && !right)
			{
				game_left_x--;
				game_right_x++;

				background_sprite[i]->xPos(background_sprite[i]->xPos() + parallax_background[i].pan_speed);
			}
			else if (game_left_x < x_pos_right_boundary && right)
			{
				game_left_x++;
				game_right_x--;
				background_sprite[i]->xPos(background_sprite[i]->xPos() - parallax_background[i].pan_speed);
			}
		}

		if (game_left_x > 0 && !right)
		{
			game_left_x--;
			game_right_x++;

			for (int j = 0; j <= menu->num_of_menus; j++)
			{
				for (int k = 0; k <= 32; k++)
				{
					if (menu->menu_storage[j].objects[k].action == "pan")
					{
						//Entities of this type will only have a single texture file.
						menu->menu_storage[j].objects[k].game_object->spriteComponent()->getSprite()->xPos(menu->menu_storage[j].objects[k].game_object->spriteComponent()->getSprite()->xPos() + 1.2);
					}
				}
			}

			//Then we must be in game.
			for (int j = 0; j <= level->getCurrentLevel()->num_of_objects; j++)
			{
				if (level->getCurrentLevel()->objects[j].game_object->spriteComponent() != nullptr)
				{
					level->getCurrentLevel()->objects[j].game_object->spriteComponent()->getSprite()->xPos(
						level->getCurrentLevel()->objects[j].game_object->spriteComponent()->getSprite()->xPos() + 2.3);
				}
				if (level->getCurrentLevel()->objects[j].despawn_game_object->spriteComponent() != nullptr)
				{
					level->getCurrentLevel()->objects[j].despawn_game_object->spriteComponent()->getSprite()->xPos(
						level->getCurrentLevel()->objects[j].despawn_game_object->spriteComponent()->getSprite()->xPos() + 2.3);
				}
			}
		}
		else if (game_left_x < x_pos_right_boundary && right)
		{
			game_left_x++;
			game_right_x--;

			for (int j = 0; j <= menu->num_of_menus; j++)
			{
				for (int k = 0; k <= 32; k++)
				{
					if (menu->menu_storage[j].objects[k].action == "pan")
					{
						//Entities of this type will only have a single texture file.
						menu->menu_storage[j].objects[k].game_object->spriteComponent()->getSprite()->xPos(menu->menu_storage[j].objects[k].game_object->spriteComponent()->getSprite()->xPos() - 1.2);
					}
				}
			}

			//Then we must be in game.
			for (int j = 0; j <= level->getCurrentLevel()->num_of_objects; j++)
			{
				if (level->getCurrentLevel()->objects[j].game_object->spriteComponent() != nullptr)
				{
					level->getCurrentLevel()->objects[j].game_object->spriteComponent()->getSprite()->xPos(
						level->getCurrentLevel()->objects[j].game_object->spriteComponent()->getSprite()->xPos() - 2.3);
				}
				if (level->getCurrentLevel()->objects[j].despawn_game_object->spriteComponent() != nullptr)
				{
					level->getCurrentLevel()->objects[j].despawn_game_object->spriteComponent()->getSprite()->xPos(
						level->getCurrentLevel()->objects[j].despawn_game_object->spriteComponent()->getSprite()->xPos() - 2.3);
				}
			}
		}
	}
}

void Scene::setGameWidth(int &width)
{
	game_width = width;
}

void Scene::setGameHeight(int &height)
{
	game_height = height;
}

bool Scene::canPan()
{
	return can_move_on_x;
}

void Scene::setCanPan(bool toggle_pan)
{
	can_move_on_x = toggle_pan;
}

void Scene::resetPanning()
{
	game_left_x = 0;
	game_right_x = 0;
}

bool Scene::hasLoaded()
{
	return loaded;
}