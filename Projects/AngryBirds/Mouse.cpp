#include "Mouse.h"
#include "Level.h"

Mouse::Mouse()
{
}


Mouse::~Mouse()
{
}
bool Mouse::initialize(std::unique_ptr<ASGE::Renderer> *renderer, std::unique_ptr<ASGE::Input> *input, Scene *scene, Menu *menu)
{
	if (!hasStartedInit)
	{
		hasStartedInit = true;
		this->renderer = renderer;
		this->input = input;
		this->menu = menu;

		x_pos = 0;
		y_pos = 0;

		const char *mouse_pointer_texture = "Resources\\Cursors\\png\\cursor.png";
		const char *mouse_hand_texture = "Resources\\Cursors\\png\\clicker.png";
		const char *mouse_grab_texture = "Resources\\Cursors\\png\\drag.png";

		if (!cursors[0].addSpriteComponent(renderer->get(), mouse_pointer_texture, scene))
		{
			return false;
		}
		if (!cursors[1].addSpriteComponent(renderer->get(), mouse_hand_texture, scene))
		{
			return false;
		}
		if (!cursors[2].addSpriteComponent(renderer->get(), mouse_grab_texture, scene))
		{
			return false;
		}

		for (int i = 0; i < CURSORS_AMOUNT; i++)
		{
			cursor_sprite[i] = cursors[i].spriteComponent()->getSprite();
		}

		for (int i = 0; i < BUTTONS_AMOUNT; i++)
		{
			button_is_not_released[i] = false;
		}

		lmb_status = "Released";
		rmb_status = "Released";

		cursors[0].spriteComponent()->getSprite()->xPos(0);
		cursors[0].spriteComponent()->getSprite()->yPos(0);
		cursors[1].spriteComponent()->getSprite()->xPos(0);
		cursors[1].spriteComponent()->getSprite()->yPos(0);
		cursors[2].spriteComponent()->getSprite()->xPos(0);
		cursors[2].spriteComponent()->getSprite()->yPos(0);

		setCursorType(cursor_types::CURSOR_POINTER);

		setCursorStatus(ASGE::CursorMode::HIDDEN);
		//setCursorStatus(ASGE::CursorMode::LOCKED);

		loaded = true;
	}
	return true;
}

void Mouse::action(buttons button)
{
	//If the left mouse button is being held down.
	if (button_is_not_released[button])
	{
		if (button == buttons::LEFT)
		{
			setCursorType(cursor_types::CURSOR_GRAB);

			lmb_status = "Held down";
		}
		else if (button == buttons::RIGHT)
		{
			rmb_status = "Held down";
		}
		else
		{
			setCursorType(cursor_types::CURSOR_POINTER);
		}
	}
	else if(button == buttons::LEFT)
	{
		setCursorType(cursor_types::CURSOR_POINTER);
		lmb_status = "Released";
	}
	else if (button == buttons::RIGHT)
	{
		rmb_status = "Released";
	}
	else
	{
		setCursorType(cursor_types::CURSOR_POINTER);
	}
}

void Mouse::setButton(buttons button, bool released)
{
	button_is_not_released[button] = released;
	action(button);
}

void Mouse::updateMouseScrolling(float y_offset, const ASGE::GameTime& us)
{
	if (menu->getCurrentMenu() == menu->MENU_LEVELSELECTION)
	{
		menu->updateLevelSelectionMenu(y_offset, us);
	}
}

void Mouse::hasMoved(double mouse_x, double mouse_y, Menu *menu, const ASGE::GameTime& us, Scene *scene, Level *level, bool in_level_selection, bool in_menu)
{
	x_pos = mouse_x;
	y_pos = mouse_y;

	//Only works on cursors_amount-1, needs fix. Could be that one of the cursors isn't within the array.
	for (int i = 0; i < CURSORS_AMOUNT; i++)
	{
		//Don't allow user to move the mouse along the bounds of the window.
		/*
			NOTE: Feature on-hold for now, ASGE doesn't support modifying the actual mouse pointer's position.
			Without this we still can't stop the mouse from going off the screen, where its x & y still changes regardless (this means that to
			control the mouse again the player would need to navigate the Windows mouse pointer back into the screen.
		*/
		//if (x_pos >= 0 && x_pos <= 1600 && y_pos >= 0 && y_pos <= 900)
		//{
			cursor_sprite[i]->xPos(x_pos);
			cursor_sprite[i]->yPos(y_pos);
		//}
	}

	if (button_is_not_released[buttons::LEFT])
	{
		//Check whether they're dragging right or left, and then move in the opposite direction
		if (mouse_dir == direction::MOVE_LEFT)
		{
			scene->panGameScene(true, menu, us, level, in_level_selection, in_menu);
		}
		else if (mouse_dir == direction::MOVE_RIGHT)
		{
			scene->panGameScene(false, menu, us, level, in_level_selection, in_menu);
		}
	}
}

double Mouse::getXPos()
{
	return x_pos;
}

double Mouse::getYPos()
{
	return y_pos;
}

void Mouse::setCursorType(cursor_types type)
{
	current_cursor = cursors[type];
}

ASGE::Sprite *Mouse::getSprite(int num)
{
	ASGE::Sprite *sprite = cursor_sprite[num];
	return sprite;
}

GameObject *Mouse::getGameObject(int num)
{
	return &cursors[num];
}

void Mouse::updateDirection()
{
	if (x_pos >= last_x_pos)
	{
		mouse_dir = direction::MOVE_RIGHT;
		last_x_pos = x_pos;
	}
	else if (x_pos <= last_x_pos)
	{
		mouse_dir = direction::MOVE_LEFT;
		last_x_pos = x_pos;
	}
	else
	{
		mouse_dir = direction::MOVE_STATIONARY;
		last_x_pos = x_pos;
	}
}

void Mouse::setCursorStatus(ASGE::CursorMode cursor)
{
	input->get()->setCursorMode(cursor);
}

bool Mouse::hasLoaded()
{
	return loaded;
}