#include <windows.h>
#include <fstream>
#include <iostream>
#include <sstream>
#include <regex>

#include "Level.h"
#include "Mouse.h"
#include "Menu.h"
#include "Scene.h"
#include "Physics.h"
#include "Audio.h"

Level::Level()
{
}


Level::~Level()
{
}

bool Level::initialize(std::unique_ptr<ASGE::Renderer> *renderer, Scene &scene_p, Mouse &mouse_p, Physics &physics_p, Menu &menu_p, Audio &audio)
{
	if (!hasStartedInit)
	{
		hasStartedInit = true;
		this->renderer = renderer;
		scene = &scene_p;
		mouse = &mouse_p;
		physics = &physics_p;
		menu = &menu_p;
		this->audio = &audio;
		Mathematics *maths = new Mathematics();
		level_failed = new GameObject();

		//Load in Level from files and populate the level selection menu.
		if (!getLevels() || !readyLevelSelection())
		{
			return false;
		}

		initializeLevel(0, true);

		for (int i = 0; i < 50; i++)
		{
			flight_path_markers[i] = new GameObject();
			if (!flight_path_markers[i]->addSpriteComponent(renderer->get(), "Resources\\Misc\\target.png", scene))
			{
				return false;
			}
			flight_path_markers[i]->attributes.visibility = false;
		}

		if (!level_failed->addSpriteComponent(renderer->get(), "Resources\\Misc\\level-failed.png", scene))
		{
			return false;
		}
		else
		{
			level_failed->spriteComponent()->getSprite()->xPos(604);
			level_failed->spriteComponent()->getSprite()->yPos(315);
		}

		loadChapterNames(renderer);

		loaded = true;
	}

	return true;
}
/*
	Unlike 'initialize', this is called every time a level loads so that objects are correctly set.
*/
void Level::initializeLevel(int num_level, bool initialInit)
{
	is_character_in_cannon = false;
	game_over = false;
	level_over_timer = 0;
	level_storage[num_level].score = 0;

	scene->resetPanning();
	
	if (initialInit)
	{
		for (int i = 0; i < num_of_levels; i++)
		{
			for (int j = 0; j < level_storage[i].num_of_objects; j++)
			{
				if (!level_storage[i].objects[j].game_object->addSpriteComponent(renderer->get(), level_storage[i].objects[j].texture_file_location_1, scene))
				{
					return;
				}

				if (level_storage[i].objects[j].type == "friend")
				{
					if (!level_storage[i].objects[j].despawn_game_object->addSpriteComponent(renderer->get(), "Resources\\Misc\\electric.png", scene))
					{
						return;
					}

					level_storage[num_level].objects[i].despawn_game_object->attributes.visibility = false;	
				}
				else if (level_storage[i].objects[j].type == "enemy")
				{
					/*if (!level_storage[i].objects[j].despawn_game_object->addSpriteComponent(renderer->get(), "Resources\\Textures\\Smoke\\Green\\fart05.png", scene))
					{
						return;
					}*/

					if (!level_storage[i].objects[j].despawn_game_object->addSpriteComponent(renderer->get(), "Resources\\Misc\\smoke.png", scene))
					{
						return;
					}

					level_storage[i].objects[j].despawn_game_object->spriteComponent()->getSprite()->scale(0.5);
				}

				

				if (level_storage[i].objects[j].subtype == "cannon")
				{
					cannon = level_storage[i].objects[j].game_object;
				}
				if (level_storage[i].objects[j].subtype == "cannon-component-front")
				{
					cannon_leg_front = level_storage[i].objects[j].game_object;
				}
				level_storage[i].objects[j].game_object->spriteComponent()->getSprite()->xPos(level_storage[i].objects[j].x_pos);
				level_storage[i].objects[j].game_object->spriteComponent()->getSprite()->yPos(level_storage[i].objects[j].y_pos);

				level_storage[i].objects[j].game_object->attributes.visibility = true;
				level_storage[i].objects[j].game_object->attributes.current_bezier_position = 0;
				level_storage[i].objects[j].game_object->attributes.bezier_animation_done = false;
				level_storage[i].objects[j].game_object->attributes.direction.speed = 0;
				level_storage[i].objects[j].game_object->attributes.spent_timer = 0;
				level_storage[i].objects[j].game_object->attributes.physical_params.weight = level_storage[i].objects[j].weight;
				float weight = level_storage[i].objects[j].game_object->attributes.physical_params.weight;

				level_storage[num_level].objects[i].despawn_game_object->attributes.visibility = false;
			}
		}
	}
	else
	{
		for (int i = 0; i < level_storage[num_level].num_of_objects; i++)
		{
			if (level_storage[num_level].objects[i].subtype == "cannon")
			{
				cannon = level_storage[num_level].objects[i].game_object;
			}
			else if (level_storage[num_level].objects[i].subtype == "cannon-component-front")
			{
				cannon_leg_front = level_storage[num_level].objects[i].game_object;
			}

			level_storage[num_level].objects[i].game_object->spriteComponent()->getSprite()->xPos(level_storage[num_level].objects[i].x_pos);
			level_storage[num_level].objects[i].game_object->spriteComponent()->getSprite()->yPos(level_storage[num_level].objects[i].y_pos);
			level_storage[num_level].objects[i].game_object->attributes.visibility = true;
			level_storage[num_level].objects[i].game_object->attributes.current_bezier_position = 0;
			level_storage[num_level].objects[i].game_object->attributes.bezier_animation_done = false;
			level_storage[num_level].objects[i].game_object->attributes.ready_to_fire = false;
			level_storage[num_level].objects[i].game_object->attributes.direction.speed = 0;
			level_storage[num_level].objects[i].game_object->attributes.spent_timer = 0;
			level_storage[num_level].objects[i].game_object->attributes.is_spent = false;
			level_storage[num_level].objects[i].game_object->attributes.physical_params.time_stepping = 0;
			level_storage[num_level].objects[i].game_object->attributes.physical_params.weight = level_storage[num_level].objects[i].weight;
			
			if (level_storage[num_level].objects[i].despawn_game_object == nullptr)
			{
				if (level_storage[num_level].objects[i].type == "friend")
				{
					if (!level_storage[num_level].objects[i].despawn_game_object->addSpriteComponent(renderer->get(), "Resources\\Misc\\electric.png", scene))
					{
						return;
					}
					level_storage[num_level].objects[i].despawn_game_object->attributes.visibility = false;
				}
				else if (level_storage[num_level].objects[i].type == "enemy")
				{
					if (!level_storage[num_level].objects[i].despawn_game_object->addSpriteComponent(renderer->get(), "Resources\\Textures\\Smoke\\Green\\fart05.png", scene))
					{
						return;
					}
				}
			}
			else
			{
				if (level_storage[num_level].objects[i].despawn_game_object->resetSprite(renderer->get()))
				{
					level_storage[num_level].objects[i].despawn_game_object->attributes.visibility = false;
				}
			}
		}
	}

	level_timer = 0;
	is_character_in_cannon = false;
	cannon_character = nullptr;
	cannon_is_active = false;


	level_storage[num_level].is_current = true;
	current_level = &level_storage[num_level];

	readyCannon();
}

bool Level::isCurrentLevelLast()
{
	int num_of_curr_level = getLevelNum(current_level);
	if (num_of_curr_level + 1 == num_of_levels)
	{
		return true;
	}
	return false;
}

bool Level::getLevels()
{
	std::ostringstream file_name;

	int file_count = 0;
	LPCWSTR file = L"Resources\\Levels\\*.txt";
	WIN32_FIND_DATA FindFileData;
	HANDLE hFind;
	hFind = FindFirstFile(file, &FindFileData);

	struct validations 
	{
		bool is_level_source = false;
		bool critical_fail = false;
	};

	if (hFind != INVALID_HANDLE_VALUE) 
	{
		while (FindNextFile(hFind, &FindFileData))
		{
			file_count++;
		}
	}
	else
	{
		return false;
	}
	for (int i = 0; i <= file_count; i++)
	{
		file_name << "Resources\\Levels\\" << i << ".txt";
		std::ifstream file(file_name.str());
		file_name.str("");

		//Pre-define stuff here so we don't create a memory leak by creating objects we only need once.
		std::string file_contents;

		int line_number = 0;

		validations checks;
		std::ostringstream object_name;
		level_item level_items[64] = {};
		std::string level_name = "";
		std::string level_world = "";
		int startpos = 0;
		int endpos = 0;
		int num_of_items = 0;

		for (file_contents; std::getline(file, file_contents);)
		{
			if (!checks.critical_fail)
			{
				line_number++;

				//Check whether the file has been validated.
				if (checks.is_level_source)
				{
					//File has now been validated, so we simply loop through and get the data.
					if (level_name.length() < 1)
					{
						std::regex re("(_LEVELNAME_ : ')(\\d+)(:)(\\d+)(';)");
						std::smatch match;
						if (std::regex_search(file_contents, match, re) && match.size() > 1)
						{
							level_name = match.str(2);
							level_world = match.str(4);
						}
						else
						{
							checks.is_level_source = false;
							checks.critical_fail = true;
						}
					}
					else
					{
						num_of_items++;
						//Now we can loop through the actual objects.
						//We're on line three but need to populate the array from 0, so we need to subtract 3 from the line number to get the array position that we want
						level_items[line_number - 3].game_object = new GameObject();
						level_items[line_number - 3].game_object = new GameObject();
						
						try 
						{
							std::regex re(".*?((?:[a-z][a-z0-9_]*)).*?(\\\'.*?\\\').*?(\\\'.*?\\\').*?(\\\'.*?\\\').*?(\\\'.*?\\\').*?(\\\'.*?\\\').*?(\\\'.*?\\\').*?(\\\'.*?\\\')");
							std::smatch match;
 							if (std::regex_search(file_contents, match, re) && match.size() > 1)
							{
								level_items[line_number - 3].score = std::atof(match.str(8).substr(1, match.str(8).length() - 2).c_str());
								level_items[line_number - 3].weight = std::atof(match.str(7).substr(1, match.str(7).length() - 2).c_str());
								level_items[line_number - 3].subtype = match.str(6).substr(1, match.str(6).length() - 2);
								level_items[line_number - 3].type = match.str(5).substr(1, match.str(5).length() - 2);
								level_items[line_number - 3].y_pos = atoi(match.str(4).substr(1, match.str(4).length() - 2).c_str());
								level_items[line_number - 3].x_pos = atoi(match.str(3).substr(1, match.str(3).length() - 2).c_str());
								level_items[line_number - 3].texture_file_location_1 = match.str(2).substr(1, match.str(2).length() - 2);
							}
						}
						catch (int exception)
						{
							MessageBox(NULL, L"Level Engine: Syntax error. Aborting.", L"Error", MB_OK);
							return false;
						}
					}
				}
				//Is this a level source file?
				else if (file_contents.find_first_of("[") != std::string::npos && file_contents.find_last_of("]") != std::string::npos)
				{
					num_of_levels++;
					checks.is_level_source = true;
					checks.critical_fail = false;

				}
				else
				{
					checks.is_level_source = false;
					checks.critical_fail = true;
				}
			}
		}
		if (!checks.critical_fail)
		{
			//Finally, we need to store the menu so we can do stuff with it.
			level_storage[i] = {};
			level_storage[i].name = level_name;
			level_storage[i].world = level_world;

			for (int j = 0; j < num_of_items; j++)
			{
				level_storage[i].objects[j].score = level_items[j].score;
				level_storage[i].objects[j].weight = level_items[j].weight;
				level_storage[i].objects[j].subtype = level_items[j].subtype;
				level_storage[i].objects[j].type = level_items[j].type;
				level_storage[i].objects[j].texture_file_location_1 = level_items[j].texture_file_location_1;
				level_storage[i].objects[j].x_pos = level_items[j].x_pos;
				level_storage[i].objects[j].y_pos = level_items[j].y_pos;
				level_storage[i].num_of_objects = num_of_items;
			}
		}
		else
		{

			return false;
		}
	}
	return true;
}

/*
	This creates the level selection screen, and is called every time the level selection screen is shown in order to update it.
*/
bool Level::readyLevelSelection()
{
	double min_x = 75;
	double max_x = 1400;
	double min_y = 200;
	double tile_width = 160;
	double tile_height = 92;
	double tile_spacing = 20;
	double page_width = 1600;
	double world_offset = 300;
	double curr_x = min_x;
	double curr_y = min_y;
	int pages = 1;
	bool reset_x = true;
	int last_world = 1;

	std::string level_tile = "Resources\\Misc\\level-blank.png";
	std::string level_completed_texture = "Resources\\Misc\\level_complete.png";
	std::string level_locked_texture = "Resources\\Misc\\Numbers\\lock.png";

	for (int i = 0; i < num_of_levels; i++)
	{
		level *level_file = &level_storage[i];
		level_file->tile = new GameObject();
		level_file->completed_icon = new GameObject();

		level_file->is_current = false;

		if (hasUnlockedLevel(level_file))
		{
			level_file->is_locked = false;
		}
		else
		{
			level_file->is_locked = true;
		}

		if (!level_file->tile->addSpriteComponent(renderer->get(), level_tile, scene) || !level_file->completed_icon->addSpriteComponent(renderer->get(), level_completed_texture, scene))
		{
			return false;
		}

		if (level_file->is_completed)
		{
			level_file->completed_icon->attributes.visibility = true;
		}
		else
		{
			level_file->completed_icon->attributes.visibility = false;
		}
		

		/*
			If next level tile's X placement exceeds the maximum width, we create another row.
			If next level tile's Y placement exceeds the maximum height, we create another page.
		*/

		if ((curr_x + tile_width) >= max_x)
		{
			curr_x = min_x;
			curr_y += tile_spacing + tile_height;
			reset_x = true;
		}

		if (reset_x)
		{
			reset_x = false;
		}
		else
		{
			curr_x += tile_spacing + tile_width;
		}

		if (last_world != atoi(level_file->world.c_str()))
		{
			curr_x = min_x;
			curr_y += 75;
		}

		curr_y *= atoi(level_file->world.c_str());

		level_file->tile->spriteComponent()->getSprite()->xPos(curr_x);
		level_file->tile->spriteComponent()->getSprite()->yPos(curr_y);
		level_file->tile->attributes.visibility = true;
	}
	return true;
}

bool Level::hasUnlockedLevel(level *level_file)
{
	//TODO
	return true;
}

Level::level* Level::getCurrentLevel()
{
	return current_level;
}

int Level::getLevelNum(level *level_obj)
{
	for (int i = 0; i < num_of_levels; i++)
	{
		if (level_storage[i].name == level_obj->name)
		{
			return i;
		}
	}
}

void Level::renderLevel()
{
	for (int i = 0; i < 50; i++)
	{
		if (flight_path_markers[i]->attributes.visibility)
		{
			renderer->get()->renderSprite(*flight_path_markers[i]->spriteComponent()->getSprite());
		}
	}

	for (int i = 0; i < current_level->num_of_objects; i++)
	{
		if (current_level->objects[i].game_object->attributes.visibility)
		{
			renderer->get()->renderSprite(*current_level->objects[i].game_object->spriteComponent()->getSprite());
		}
		else if (current_level->objects[i].despawn_game_object->attributes.visibility)
		{
			renderer->get()->renderSprite(*current_level->objects[i].despawn_game_object->spriteComponent()->getSprite());
		}
	}
}

void Level::loadChapterNames(std::unique_ptr<ASGE::Renderer> *renderer)
{
	float y_pos = 156.0f;
	std::ostringstream chapter_file_name;
	for (int i = 0; i < CHAPTER_COUNT; i++)
	{
		chapterNames[i] = new GameObject();
		chapter_file_name << "Resources\\Misc\\Chapter Names\\" << i + 1 << ".png";
		if (!chapterNames[i]->addSpriteComponent(renderer->get(), chapter_file_name.str(), scene))
		{
			chapterNames[i] = nullptr;
			return;
		}

		chapterNames[i]->spriteComponent()->getSprite()->xPos(681.0f);
		chapterNames[i]->spriteComponent()->getSprite()->yPos(y_pos);

		chapterNames[i]->attributes.visibility = true;
		y_pos += 347.0f;

		chapter_file_name.str("");
		chapter_file_name.clear();
	}
}

GameObject *Level::getChapterName(std::string name)
{
	int chapter_i = atoi(name.c_str());
	if (chapterNames[chapter_i] != nullptr)
	{
		return chapterNames[chapter_i];
	}
	return nullptr;
}

GameObject *Level::getChapterName(int chapter_i)
{
	if (chapterNames[chapter_i] != nullptr)
	{
		return chapterNames[chapter_i];
	}
	return nullptr;
}

int Level::getChapterId(GameObject *chapter)
{
	for (int i = 0; i < CHAPTER_COUNT; i++)
	{
		if (chapterNames[i]->getTextureFile() == chapter->getTextureFile())
		{
			return i;
		}
	}
	return 0;
}

std::string Level::getChapterIdAsString(GameObject *chapter)
{
	int id = getChapterId(chapter) + 1;

	switch (id)
	{
		case (1):
		{
			return "ONE";
		}
		case (2):
		{
			return "TWO";
		}
		case (3):
		{
			return "THREE";
		}
		case (4):
		{
			return "FOUR";
		}
		case (5):
		{
			return "FIVE";
		}
		case (6):
		{
			return "SIX";
		}
		case (7):
		{
			return "SEVEN";
		}
		case (8):
		{
			return "EIGHT";
		}
	}

	return "UNKNOWN";
}

void Level::readyCannon()
{
	for (int i = 0; i < num_of_levels; i++)
	{
		for (int j = 0; j < level_storage[i].num_of_objects; j++)
		{
			//Look for a cannon.
			if (level_storage[i].objects[j].subtype == "cannon")
			{
				rotateAroundSprite(60, cannon, level_storage[i].objects[j].x_pos + cannon_leg_front->spriteComponent()->getSprite()->width()/2 - 5, level_storage[i].objects[j].y_pos-25);

				//Technically a level could have more than one slingshot but we've programmed only for a single cannon, so break here so we don't ready any other cannon present in the level.
				break;
			}
		}
	}
}

void Level::showFlightPath(GameObject *object)
{
	for (int i = 0; i < 50; i++)
	{
		flight_path_markers[i]->attributes.visibility = true;
		flight_path_markers[i]->spriteComponent()->getSprite()->xPos(object->attributes.bezier_positions[i * 4].x);

		//If the path goes through the ground, then do not show the path beyond the ground.
		if (object->attributes.bezier_positions[i * 4].y > 820)
		{
			flight_path_markers[i]->attributes.visibility = false;
		}
		else
		{
			flight_path_markers[i]->spriteComponent()->getSprite()->yPos(object->attributes.bezier_positions[i * 4].y);
		}
	}
}

void Level::hideFlightPath(GameObject *object)
{
	for (int i = 0; i < 50; i++)
	{
		flight_path_markers[i]->attributes.visibility = false;
	}
}

/*
	Effectively the core game input processing function.
	Responsible for interpreting input and responding.
*/
void Level::processInput()
{
	//Check distance between mouse cursor and cannon (since we can't require the player to hover exactly over the slingshot to fire it).

	double distance = Mathematics::getDistance(mouse->current_cursor.spriteComponent()->getSprite(), cannon->spriteComponent()->getSprite());
	cannon_angle = Mathematics::getAngle(mouse->current_cursor.spriteComponent()->getSprite(), cannon->spriteComponent()->getSprite()) - 90;

	Mathematics::pos mouse_pos;
	Mathematics::pos character_pos;
	Mathematics::pos floor_pos;

	if ((distance <= 200 && mouse->button_is_not_released[Mouse::buttons::LEFT]) || (cannon_is_active && mouse->button_is_not_released[Mouse::buttons::LEFT]) && !cannon_character->attributes.has_fired)
	{
		scene->setCanPan(false);
		cannon_is_active = true;

		mouse->setCursorType(Mouse::cursor_types::CURSOR_GRAB);
		for (int j = 0; j < current_level->num_of_objects; j++)
		{
			/*if (cannon_angle > 40 || cannon_angle < -220)
			{ */
				cannon->spriteComponent()->getSprite()->rotationInRadians(Mathematics::getAngleInRadians(mouse->current_cursor.spriteComponent()->getSprite(),
					cannon->spriteComponent()->getSprite()) - 90);
				rotateAroundSprite(Mathematics::radToDeg(cannon->spriteComponent()->getSprite()->rotationInRadians()), cannon_character,
					(cannon_leg_front->spriteComponent()->getSprite()->xPos() + 25),
					cannon_leg_front->spriteComponent()->getSprite()->yPos() - 10);

				mouse_pos.x = mouse->getXPos();
				mouse_pos.y = mouse->getYPos();

				character_pos.x = cannon->spriteComponent()->getSprite()->xPos() + 60;
				character_pos.y = cannon->spriteComponent()->getSprite()->yPos();

				floor_pos.y = 826;

				physics->calculateBezierTrajectory(cannon_character, mouse_pos, floor_pos, character_pos, 900, 1600);
				showFlightPath(cannon_character);
			//}
		}
	}
	else
	{
		if ((cannon_is_active && !mouse->button_is_not_released[Mouse::buttons::LEFT]))
		{
			cannon_character->attributes.ready_to_fire = true;
			cannon_character->attributes.bezier_animation_done = false;
			cannon_is_active = false;
		}
		hideFlightPath(cannon_character);
		scene->setCanPan(true);
	}
}

void Level::doAnimations(double dt_sec)
{
	level_timer += dt_sec;
	/*
		If there is no character in the cannon, then we need to put one in. Note that this is not nullified until the character has died.
		This means that the character won't be loaded until the last one has been used.
	*/
	if (!is_character_in_cannon)
	{ 
		for (int j = 0; j <= current_level->num_of_objects; j++)
		{
			//Find the next available character. 
			if (current_level->objects[j].type == "friend" && !current_level->objects[j].game_object->attributes.physical_params.time_stepping > 0)
			{
				cannon_character = current_level->objects[j].game_object;
				cannon_character->attributes.visibility = false;
				is_character_in_cannon = true;
				break;
			}	
		}
	}
	else
	{
		if (!cannon_character->attributes.bezier_animation_done)
		{
			// Fire the character.
			if (cannon_character->attributes.ready_to_fire)
			{
				cannon_character->attributes.visibility = true;
				cannon_character->attributes.has_fired = true;
				if (cannon_character->attributes.current_bezier_position < 200)
				{
					double bezier_pos_x = cannon_character->attributes.bezier_positions[cannon_character->attributes.current_bezier_position].x;
					double bezier_pos_y = cannon_character->attributes.bezier_positions[cannon_character->attributes.current_bezier_position].y;

					if (bezier_pos_y + cannon_character->spriteComponent()->getSprite()->height() > 826)
					{
						cannon_character->attributes.current_bezier_position = 0;
						cannon_character->attributes.bezier_animation_done = true;
						cannon_character->attributes.is_spent = true;
						cannon_character->attributes.can_roll = true;
					}
					else
					{
						cannon_character->attributes.direction.normalise();

						double x_pos = 0;
						double y_pos = 0;

						int vec_x = 0;
						int vec_y = 0;

						if (bezier_pos_x >= cannon_character->spriteComponent()->getSprite()->xPos())
						{
							cannon_character->attributes.direction.x = 1;
							cannon_character->attributes.current_bezier_position++;
						}
						else
						{
							cannon_character->attributes.direction.x = -1;
						}
						if (bezier_pos_y >= cannon_character->spriteComponent()->getSprite()->yPos())
						{
							cannon_character->attributes.direction.y = 1;
						}
						else
						{
							cannon_character->attributes.direction.y = -1;
						}

						Mathematics::pos new_position = physics->calculateGravitySpeedInterpolation(*cannon_character, true, bezier_pos_x, bezier_pos_y, cannon_character->attributes.direction.x,
							cannon_character->attributes.direction.y);

						cannon_character->spriteComponent()->getSprite()->xPos(new_position.x);
						cannon_character->spriteComponent()->getSprite()->yPos(new_position.y);

						if (new_position.x >= bezier_pos_x)
						{
							cannon_character->attributes.current_bezier_position++;
						}
					}
					
				}
				else
				{
					cannon_character->attributes.current_bezier_position = 0;
					cannon_character->attributes.bezier_animation_done = true;
					cannon_character->attributes.is_spent = true;
					cannon_character->attributes.can_roll = true;
					is_character_in_cannon = false;
				}
			}
		}
		
		doObjectSticky(cannon_character);
		if (cannon_character->attributes.direction.x != 0 || cannon_character->attributes.direction.y != 0)
		{
			Mathematics::pos new_position = physics->calculateGravitySpeedInterpolation(*cannon_character, true, cannon_character->spriteComponent()->getSprite()->xPos(), 
				cannon_character->spriteComponent()->getSprite()->yPos(), cannon_character->attributes.direction.x,
				cannon_character->attributes.direction.y);

			cannon_character->spriteComponent()->getSprite()->xPos(new_position.x);
			cannon_character->spriteComponent()->getSprite()->yPos(new_position.y);
		}
	}

	int num_of_visible_enemies = 0;
	int num_of_visible_friendlies = 0;

	for (int i = 0; i < current_level->num_of_objects; i++)
	{
		/*
			We need to check if the cannon character is invisible and the object matches the cannon character too.
			This is because if a friendly character dies and there is only a single friendly character left, AND they're in the cannon, the game will trigger a "game over" 
			event because it'll think none of the friendlies are alive.

			This happens because friendly characters are invisible when they're sitting in the cannon.
		*/
		if (current_level->objects[i].game_object->attributes.visibility || (!cannon_character->attributes.is_spent && !cannon_character->attributes.visibility
			&& current_level->objects[i].game_object == cannon_character))
		{
			if ((current_level->objects[i].type == "friend"))
			{
				num_of_visible_friendlies++;
			}
			else if ((current_level->objects[i].type == "enemy"))
			{
				num_of_visible_enemies++;
			}
		}
		
		if (current_level->objects[i].game_object->attributes.is_spent)
		{
			//Control explosion + disposal of character.
			if (current_level->objects[i].game_object->attributes.spent_timer == 0)
			{
				current_level->objects[i].game_object->attributes.spent_timer = (level_timer + 3);
			}

			//We only want this to apply for friendly characters right now, not enemies. We want enemies to go out with a boom.
			if (current_level->objects[i].game_object->attributes.spent_timer <= level_timer && current_level->objects[i].type == "friend")
			{
				current_level->objects[i].game_object->attributes.can_roll = false;

				current_level->objects[i].despawn_game_object->spriteComponent()->getSprite()->xPos(current_level->objects[i].game_object->spriteComponent()->getSprite()->xPos() -
					current_level->objects[i].game_object->spriteComponent()->getSprite()->width() / 3);
				current_level->objects[i].despawn_game_object->spriteComponent()->getSprite()->yPos(current_level->objects[i].game_object->spriteComponent()->getSprite()->yPos() -
					current_level->objects[i].game_object->spriteComponent()->getSprite()->height() / 3);
				current_level->objects[i].despawn_game_object->spriteComponent()->getSprite()->width(current_level->objects[i].game_object->spriteComponent()->getSprite()->width() * 3);
				current_level->objects[i].despawn_game_object->spriteComponent()->getSprite()->height(current_level->objects[i].game_object->spriteComponent()->getSprite()->height() * 3);

				if (current_level->objects[i].despawn_game_object->spriteComponent()->getSprite()->scale() > 0.0f)
				{
					current_level->objects[i].despawn_game_object->spriteComponent()->getSprite()->scale(current_level->objects[i].despawn_game_object->spriteComponent()->getSprite()->scale() -
					(5.25 * dt_sec));
				}

				if (current_level->objects[i].game_object->attributes.visibility)
				{
					current_level->objects[i].game_object->audioComponent->queueAudio("Resources\\Audio\\buzz.ogg", false, true, false);
					current_level->objects[i].game_object->attributes.visibility = false;
					current_level->objects[i].despawn_game_object->attributes.visibility = true;
					is_character_in_cannon = false;
				}

				if (current_level->objects[i].despawn_game_object->spriteComponent()->getSprite()->opacity() <= 0)
				{
					current_level->objects[i].despawn_game_object->attributes.visibility = false;
					current_level->objects[i].game_object->attributes.visibility = false;
				}
			}

			if (current_level->objects[i].type == "enemy")
			{

				current_level->objects[i].despawn_game_object->spriteComponent()->getSprite()->xPos(current_level->objects[i].game_object->spriteComponent()->getSprite()->xPos() -
					current_level->objects[i].game_object->spriteComponent()->getSprite()->width() / 3);
				current_level->objects[i].despawn_game_object->spriteComponent()->getSprite()->yPos(current_level->objects[i].game_object->spriteComponent()->getSprite()->yPos() -
					current_level->objects[i].game_object->spriteComponent()->getSprite()->height() / 3);
				current_level->objects[i].despawn_game_object->spriteComponent()->getSprite()->width(current_level->objects[i].game_object->spriteComponent()->getSprite()->width() * 3);
				current_level->objects[i].despawn_game_object->spriteComponent()->getSprite()->height(current_level->objects[i].game_object->spriteComponent()->getSprite()->height() * 3);

				current_level->objects[i].despawn_game_object->spriteComponent()->getSprite()->scale(current_level->objects[i].despawn_game_object->spriteComponent()->getSprite()->scale() +
					(float)(2.25 * dt_sec));

				current_level->objects[i].despawn_game_object->setOpacity(current_level->objects[i].despawn_game_object->spriteComponent()->getSprite()->opacity() -
					(float)(0.25 * dt_sec));

				if (current_level->objects[i].game_object->attributes.visibility)
				{
					current_level->objects[i].game_object->audioComponent->queueAudio("Resources\\Audio\\pop.ogg", false, true, false);
					current_level->objects[i].game_object->attributes.visibility = false;
					current_level->objects[i].despawn_game_object->attributes.visibility = true;
				}



				if (current_level->objects[i].despawn_game_object->spriteComponent()->getSprite()->opacity() <= 0)
				{
					current_level->objects[i].despawn_game_object->attributes.visibility = false;
					current_level->objects[i].despawn_game_object->setOpacity(100);
				}
			}
		}

		if (current_level->objects[i].game_object->attributes.can_roll)
		{
			current_level->objects[i].game_object->attributes.direction.normalise();

			//For now, only roll on the X axis.
			current_level->objects[i].game_object->spriteComponent()->getSprite()->xPos(current_level->objects[i].game_object->spriteComponent()->getSprite()->xPos() - -1*(current_level->objects[i].game_object->attributes.direction.speed * dt_sec));
			current_level->objects[i].game_object->spriteComponent()->getSprite()->rotationInRadians(current_level->objects[i].game_object->spriteComponent()->getSprite()->rotationInRadians() + physics->calculateRotationsPerUpdate(*current_level->objects[i].game_object));
		}
	}

	if (num_of_visible_enemies < 1 || num_of_visible_friendlies < 1)
	{
		//We wait a couple of seconds after the last enemy has been destroyed, before showing the game over menu.
		if (level_over_timer == 0)
		{
			level_over_timer = (level_timer + 2);
		}

		if (level_over_timer <= level_timer)
		{
			setGameOver(true);
		}
	}
}

void Level::setGameOver(bool is_game_over)
{
	if (is_game_over)
	{
		int amount_of_enemies = 0;
		int amount_of_friendlies = 0;
		for (int i = 0; i < current_level->num_of_objects; i++)
		{
			current_level->objects[i].game_object->audioComponent->onUpdate();
			if (current_level->objects[i].type == "enemy" && current_level->objects[i].game_object->attributes.visibility)
			{
				amount_of_enemies++;
			}
			else if (current_level->objects[i].type == "friend" && current_level->objects[i].game_object->attributes.visibility)
			{
				amount_of_friendlies++;
			}
		}

		if (amount_of_friendlies >= amount_of_enemies && amount_of_enemies == 0)
		{
			current_level->is_completed = true;
			current_level->is_completed_this_round = true;
		}
		else
		{
			current_level->is_completed_this_round = false;
		}

		menu->setCurrentMenu(menu->menus::MENU_LEVELOVER);
	}
	else
	{
		menu->setCurrentMenu(menu->menus::MENU_INGAME);
	}

	game_over = is_game_over;
}

void Level::onUpdate(double dt_sec)
{
	doAnimations(dt_sec);
	processInput();

	if (!game_over)
	{
		for (int i = 0; i < current_level->num_of_objects; i++)
		{
			current_level->objects[i].game_object->audioComponent->onUpdate();
			if ((current_level->objects[i].type == "enemy" || current_level->objects[i].type == "block") && current_level->objects[i].game_object->attributes.visibility)
			{
				for (int j = 0; j < current_level->num_of_objects; j++)
				{
					if (current_level->objects[j].type == "friend" && current_level->objects[j].game_object->attributes.visibility)
					{
						if (current_level->objects[j].game_object->spriteComponent()->getBoundingBox().isInside(current_level->objects[i].game_object->spriteComponent()->getBoundingBox()) ||
							current_level->objects[j].despawn_game_object->spriteComponent()->getBoundingBox().isInside(current_level->objects[i].game_object->spriteComponent()->getBoundingBox()) &&
							(current_level->objects[i].game_object->attributes.visibility || current_level->objects[i].despawn_game_object->attributes.visibility))
						{
							double calculated_score = current_level->objects[j].score*current_level->objects[i].score;
							current_level->score += calculated_score;
							if (current_level->objects[i].type != "block")
							{
								current_level->objects[i].game_object->attributes.spent_timer = level_timer;
								current_level->objects[i].game_object->attributes.is_spent = true;
							}
							else
							{
								current_level->objects[i].game_object->attributes.visibility = false;
							}

							current_level->objects[j].game_object->attributes.spent_timer = level_timer;
							current_level->objects[j].game_object->attributes.is_spent = true;
						}
					}
				}
			}
		}
	}
}

void Level::doObjectSticky(GameObject *cannon_character)
{
	for (int i = 0; i < current_level->num_of_objects; i++)
	{
		if (current_level->objects[i].type == "block" && current_level->objects[i].game_object->attributes.visibility)
		{
			if (cannon_character->spriteComponent()->getBoundingBox().isInside(current_level->objects[i].game_object->spriteComponent()->getBoundingBox())
				&& current_level->objects[i].game_object->attributes.visibility)
			{
				cannon_character->attributes.direction.x = 0;
				cannon_character->attributes.direction.y = 0;
				cannon_character->attributes.current_bezier_position = 0;
				cannon_character->attributes.bezier_animation_done = true;
				cannon_character->attributes.is_spent = true;
				is_character_in_cannon = false;

				return;
			}
		}
	}
}

/*
	Rotate an object using its origin as its right x and y coords.
*/
void Level::rotateAroundSprite(double degrees, GameObject *sprite, double x_pos, double y_pos)
{
	double pivot_x = sprite->spriteComponent()->getSprite()->xPos() + (sprite->spriteComponent()->getSprite()->width() / 2);
	double pivot_y = sprite->spriteComponent()->getSprite()->yPos() + (sprite->spriteComponent()->getSprite()->height() / 2);
	pivot_x -= sprite->spriteComponent()->getSprite()->xPos();
	pivot_y -= sprite->spriteComponent()->getSprite()->yPos();

	double radians = degrees * (3.14 / 180.0);

	double sprite_x = sprite->spriteComponent()->getSprite()->xPos();
	double sprite_y = sprite->spriteComponent()->getSprite()->yPos();

	float new_x_pos = (pivot_x * std::cos(radians) - pivot_y * std::sin(radians)) + x_pos;
	float new_y_pos = (pivot_x * std::sin(radians) + pivot_y * std::cos(radians)) + (y_pos - 22);

	sprite->spriteComponent()->getSprite()->xPos(new_x_pos);
	sprite->spriteComponent()->getSprite()->yPos(new_y_pos);

	sprite->spriteComponent()->getSprite()->rotationInRadians(radians);
}

float Level::getCannonCharacterVelocity()
{
	if (cannon_character != nullptr)
	{ 
		return cannon_character->attributes.direction.speed;
	}
	else
	{
		return 0;
	}
}

std::string Level::getCurrentWorld()
{
	return current_level->world;
}

int Level::getNumOfCompletedLevels()
{
	int completed_count = 0;
	for (int i = 0; i <= num_of_levels; i++)
	{
		if (level_storage[i].is_completed)
		{
			completed_count++;
		}
	}
	return completed_count;
}

int Level::getNumOfCompletedLevelsForWorld(int world)
{
	int completed_count = 0;
	int num_level = 0;
	for (int i = 0; i <= num_of_levels; i++)
	{
		num_level = atoi(level_storage[i].world.c_str());
		if ((num_level == (world + 1)) && level_storage[i].is_completed)
		{
			completed_count++;
		}
	}
	return completed_count;
}

int Level::getNumOfLevelsInWorld(std::string world)
{
	int level_count = 0;
	for (int i = 0; i <= num_of_levels; i++)
	{
		if (level_storage[i].world == world)
		{
			level_count++;
		}
	}
	return level_count;
}

int Level::getNumOfLevelsInWorld(int world)
{
	int count = 0;
	int num_level = 0;
	for (int i = 0; i <= num_of_levels; i++)
	{
		num_level = atoi(level_storage[i].world.c_str());
		if (num_level == (world + 1))
		{
			count++;
		}
	}
	return count;
}

double Level::getCurrentScore()
{
	return current_level->score;
}

bool Level::hasLoaded()
{
	return loaded;
}

void Level::setPaused(bool paused)
{
	if (this->paused != paused)
	{
		this->paused = paused;
	}
	
}

bool Level::isGamePaused()
{
	return paused;
}
