/*
	This class is responsible for all scene actions, such as loading new levels and moving the level camera.

	Created: 15th Febuary 2018 @ 12:37
	Author: Alexander Stopher
*/

#pragma once

#include <Engine/Sprite.h>
#include <Engine/Gametime.h>
#include <GameObject.h>
#include "..\Projects\AngryBirds\Menu.h"

class GameObject;

class Scene
{
public:
	Scene();
	~Scene();

	static const int NUM_OF_BACKGROUNDS = 5;

	bool initialize(std::unique_ptr<ASGE::Renderer> *renderer); //Initialize the scene.

	ASGE::Sprite *background_sprite[NUM_OF_BACKGROUNDS];
	GameObject background[NUM_OF_BACKGROUNDS];

	double maximum_pan_width = 0;
	
	double game_left_x = 0.0;
	double game_right_x = 0.0;
	bool is_sound_muted;

	void panGameScene(bool right, Menu *menu, const ASGE::GameTime& us, Level *level, bool &in_level_selection, bool &in_game);
	void setGameWidth(int &width);
	void setGameHeight(int &width);
	bool canPan();
	void setCanPan(bool toggle_pan);
	void resetPanning();
	bool hasLoaded();
private:
	std::unique_ptr<ASGE::Renderer> *renderer = nullptr;

	struct background_parallax
	{
		const char *texture;
		double x_pos = 0;
		double y_pos = 0;
		double pan_speed;
	};

	background_parallax parallax_background[NUM_OF_BACKGROUNDS] = {};
	bool can_move_on_x;

	double game_width;
	double game_height;
	bool loaded = false;
	bool hasStartedInit = false;
};

