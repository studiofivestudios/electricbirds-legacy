#include <fstream>
#include <sstream>
#include <regex>

#include "Menu.h"
#include "Mouse.h"
#include "Scene.h"
#include "Level.h"
#include "Audio.h"

Menu::Menu()
{
}


Menu::~Menu()
{
}

bool Menu::initialize(std::unique_ptr<ASGE::Renderer> *renderer, Scene *scene, Level *level)
{
	this->scene = scene;
	this->level = level;
	if (!hasStartedInit)
	{
		hasStartedInit = true;
		this->renderer = renderer;

		//Load in the main menu from file.
		if (getMenu(menus::MENU_MAINMENU) == NULL || getMenu(menus::MENU_INGAME) == NULL || getMenu(menus::MENU_LEVELSELECTION) == NULL || getMenu(menus::MENU_PAUSED) == NULL ||
			getMenu(menus::MENU_LEVELOVER) == NULL || getMenu(menus::MENU_PROFILES) == NULL || getMenu(menus::MENU_HOW_TO_PLAY) == NULL)
		{
			return false;
		}

		for (int i = 0; i <= num_of_menus; i++)
		{
			try
			{
				for (int j = 0; j <= 32; j++)
				{
					menu_item item = menu_storage[i].objects[j];

					if (item.texture_file_location_1.length() < 1)
					{
						break;
					}

					if (item.game_object->spriteComponent() != nullptr)
					{
						item.game_object->resetSprite(renderer->get());
					}
					else if (!item.game_object->addSpriteComponent(renderer->get(), item.texture_file_location_1, scene))
					{
						return false;
					}

					//item.game_object->spriteComponent()->getSprite()->getTexture()->setFormat(ASGE::Texture2D::Format::RGBA);

					item.game_object->spriteComponent()->getSprite()->xPos(item.x_pos);
					item.game_object->spriteComponent()->getSprite()->yPos(item.y_pos);

					item.game_object->attributes.visibility = false;
					item.state = states::STATE_NORMAL;

					//This means that the object will pan at the same time the rest of the scene is panned.
				}
			}
			catch (int e)
			{
				//We already know what the error is (out of bounds for the array, since we reserve 32 and probably don't need that much).
				break;
			}
		}

		//setCurrentMenu(menus::MENU_MAINMENU);

		loaded = true;
	}
	return true;
}

Menu::menu_item *Menu::getMenu(menus menu)
{
	/*
		We create the menus (and game objects elsewhere) from files containing data such as the location of the texture file and positions for each menu item.
		Creation is done in this order:
			1. Check that line 1 has '/* Menu Source file.', which tells us that it's a file type that we can read.
			2. Check line 2, which has the name of the menu, extract this via a substring and then check that it's the menu file we want, and THEN store it.
			3. Loop through the rest of the lines, which contain an object on each line in the format of:
				_OBJ[number]_ : r1='[texture file 1 location]', r1='[texture file 2 location]', x='[x position]', y='[y position]';
				For each object we create a new menu object and extract this info via substr, and then we return this array.
				We store two texture files because these menu items are typically buttons, so we need one for hover and one for normal state.
			WARNING: Each menu has a maximum of 32 objects.
	 */
	std::ostringstream file_name;
	file_name << "Resources\\Menus\\" << menu << ".txt";
	std::ifstream file(file_name.str());

	//Pre-define stuff here so we don't create a memory leak by creating objects we only need once.
	std::string file_contents;

	int line_number = 0;

	struct validations 
	{
		bool is_menu_source = false;
		std::string menu_name = "";

		bool critical_fail = false;
	};

	validations checks;
	std::ostringstream object_name;
	menu_item menu_items[32] = {};
	int startpos = 0;
	int endpos = 0;
	int num_of_items = 0;

	for (file_contents; std::getline(file, file_contents);)
	{
		if (!checks.critical_fail)
		{
			line_number++;

			//Check whether the file has been validated.
			if (checks.is_menu_source)
			{
				//File has now been validated, so we simply loop through and get the data.
				if (checks.menu_name.length() < 1)
				{
					if (file_contents.find("_MENUNAME_") != std::string::npos)
					{
						startpos = file_contents.find_first_of(":'") +3;
						endpos = file_contents.length() - startpos - 2;
						checks.menu_name = file_contents.substr(startpos, endpos);
					}
					else
					{
						checks.is_menu_source = false;
						checks.critical_fail = true;
					}
				}
				else
				{
					num_of_items++;
					//Now we can loop through the actual objects.
					//We're on line three but need to populate the array from 0, so we need to subtract 3 from the line number to get the array position that we want
					menu_items[line_number - 3].game_object = new GameObject();
					menu_items[line_number - 3].game_object = new GameObject();

					std::regex re(".*?((?:[a-z][a-z0-9_]*)).*?(\\'.*?\\').*?(\\'.*?\\').*?(\\'.*?\\').*?(\\'.*?\\')");
					std::smatch match;
					if (std::regex_search(file_contents, match, re) && match.size() > 1)
					{
						menu_items[line_number - 3].action = match.str(5).substr(1, match.str(5).length()-2); 
						menu_items[line_number - 3].y_pos = atoi(match.str(4).substr(1, match.str(4).length() - 2).c_str());
						menu_items[line_number - 3].x_pos = atoi(match.str(3).substr(1, match.str(3).length() - 2).c_str());
						menu_items[line_number - 3].texture_file_location_1 = match.str(2).substr(1, match.str(2).length() - 2);
					}
					else {
						checks.critical_fail = false;
					}
				}
			}
			//Is this a menu source file?
			else if (file_contents.find_first_of("[") != std::string::npos && file_contents.find_last_of("]") != std::string::npos)
			{
				num_of_menus++;
				checks.is_menu_source = true;
				checks.critical_fail = false;

			}
			else
			{
				checks.is_menu_source = false;
				checks.critical_fail = true;
			}
		}
	}
	if (!checks.critical_fail)
	{
		//Finally, we need to store the menu so we can do stuff with it.
		menu_storage[num_of_menus - 1] = {};
		menu_storage[num_of_menus - 1].name = menu;

		for (int i = 0; i < num_of_items; i++)
		{
			menu_storage[num_of_menus - 1].objects[i].action = menu_items[i].action;
			menu_storage[num_of_menus - 1].objects[i].texture_file_location_1 = menu_items[i].texture_file_location_1;
			menu_storage[num_of_menus - 1].objects[i].x_pos = menu_items[i].x_pos;
			menu_storage[num_of_menus - 1].objects[i].y_pos = menu_items[i].y_pos;
		}
		return menu_items;
	}
	else
	{
		return NULL;
	}
	return NULL;
}

void Menu::processClick(Mouse *mouse, Scene *scene, Level *level, bool &in_menu, bool &paused, bool &in_level_selection, Audio *audio)
{
	for (int i = 31; i >= 0; i--)
	{
		if (menu_storage[getCurrentMenu()].objects[i].game_object->attributes.visibility)
		{
			if (menu_storage[getCurrentMenu()].objects[i].texture_file_location_1.length() > 1) {
				if (menu_storage[getCurrentMenu()].objects[i].action != "pan")
				{
					if (mouse->current_cursor.spriteComponent()->getBoundingBox().isInside(menu_storage[getCurrentMenu()].objects[i].game_object->spriteComponent()->getBoundingBox()) &&
						menu_storage[getCurrentMenu()].objects[i].action != "background")
					{
						std::string bg = menu_storage[getCurrentMenu()].objects[i].action;
						if (audio->isFileInQueue("Resources\\Audio\\click.ogg") == -1)
						{
							audio->queueAudio("Resources\\Audio\\click.ogg", false, true, false);
						}

						std::string action = menu_storage[getCurrentMenu()].objects[i].action;
						if (action == "main-menu")
						{
							setCurrentMenu(menus::MENU_MAINMENU);
						}
						else if (action == "profiles")
						{
							setCurrentMenu(menus::MENU_PROFILES);
						}
						else if (action == "play")
						{
							setCurrentMenu(menus::MENU_LEVELSELECTION);
						}
						else if (action == "level-resume")
						{
							level->setPaused(false);
							setCurrentMenu(menus::MENU_INGAME);
						}
						else if (action == "restart-level")
						{
							level->initializeLevel(level->getLevelNum(level->getCurrentLevel()), false);
							setCurrentMenu(menus::MENU_INGAME);
						}
						else if (action == "how-to-play")
						{
							setCurrentMenu(menus::MENU_HOW_TO_PLAY);
						}
						else if (action == "sound-mute")
						{
							audio->toggleMute(false);
							for (int j = 31; j >= 0; j--)
							{
								if (menu_storage[MENU_MAINMENU].objects[j].action == "sound-mute")
								{
									menu_storage[MENU_MAINMENU].objects[j].game_object->attributes.visibility = false;
								}

								if (menu_storage[MENU_MAINMENU].objects[j].action == "sound-on")
								{
									menu_storage[MENU_MAINMENU].objects[j].game_object->attributes.visibility = true;
								}
							}
						}
						else if (action == "sound-on")
						{
							audio->toggleMute(true);
							for (int j = 31; j >= 0; j--)
							{
								if (menu_storage[MENU_MAINMENU].objects[j].action == "sound-on")
								{
									menu_storage[MENU_MAINMENU].objects[j].game_object->attributes.visibility = false;
								}

								if (menu_storage[MENU_MAINMENU].objects[j].action == "sound-mute")
								{
									menu_storage[MENU_MAINMENU].objects[j].game_object->attributes.visibility = true;
								}
							}
						}
						else if (action == "level-selection")
						{
							setCurrentMenu(menus::MENU_LEVELSELECTION);
						}
						else if (action == "next-level")
						{
							level->initializeLevel(level->getLevelNum(level->getCurrentLevel()) + 1, false);
							setCurrentMenu(menus::MENU_INGAME);
						}
						else if (action == "exit")
						{
							exit(0);
						}
						bool continue_loop = false;
						break;
					}
				}
			}
		}
	}

	//We also process clicks for the dynamically-generated level selection screen here.

	if (getCurrentMenu() == MENU_LEVELSELECTION)
	{
		for (int i = 0; i < level->num_of_levels; i++)
		{
			if (level->level_storage[i].tile->attributes.visibility)
			{
				level->level_storage[i].is_current = false;
			}
		}
		for (int i = 0; i < level->num_of_levels; i++)
		{
			if (mouse->current_cursor.spriteComponent()->getBoundingBox().isInside(level->level_storage[i].tile->spriteComponent()->getBoundingBox()))
			{
				if (level->level_storage[i].tile->attributes.visibility)
				{
					setCurrentMenu(menus::MENU_INGAME);
					level->initializeLevel(i, false);
					scene->setCanPan(true);
					break;
				}
			}
		}
	}
}

void Menu::updateLevelSelectionMenu(float y_offset, const ASGE::GameTime& us)
{
	float dt_sec = us.delta_time.count() / 1000.0f;
	float y_pos = -1.0f;
	float speed = 16000.0f;

	float y_lower = 121.0f;
	float y_upper = 720.0f;
	bool can_scroll_up = true;
	bool can_scroll_down = true;

	int closest = 0;

	for (int i = 0; i < level->CHAPTER_COUNT; i++)
	{
		if (abs(level->chapterNames[i]->spriteComponent()->getSprite()->yPos() - y_lower) < abs(level->chapterNames[closest]->spriteComponent()->getSprite()->yPos() - y_lower))
		{
			if (level->chapterNames[i]->spriteComponent()->getSprite()->yPos() > y_lower)
			{
				closest = i;
			}
		}
	}
	
	level->chapter_name_closest_to_top = *level->chapterNames[closest];

	if ((level->chapterNames[0]->spriteComponent()->getSprite()->yPos() > (y_lower + 50)) ||
		level->chapterNames[level->CHAPTER_COUNT - 1]->spriteComponent()->getSprite()->yPos() < (y_lower + 50))
	{
		if ((level->chapterNames[0]->spriteComponent()->getSprite()->yPos() > (y_lower - 50)))
		{
			can_scroll_down = false;
		}
		if (level->chapterNames[level->CHAPTER_COUNT - 1]->spriteComponent()->getSprite()->yPos() < (y_lower + 50))
		{
			can_scroll_up = false;
		}
	}
	else
	{
		can_scroll_up = true;
		can_scroll_down = true;
	}


	for (int i = 0; i < level->CHAPTER_COUNT; i++)
	{
		if (y_offset > 0 && can_scroll_down)
		{
			y_pos = level->chapterNames[i]->spriteComponent()->getSprite()->yPos() + (speed * dt_sec);
		}
		else if (y_offset < 0 && can_scroll_up)
		{
			y_pos = level->chapterNames[i]->spriteComponent()->getSprite()->yPos() - (speed * dt_sec);
		}

		if ((y_pos < y_lower || y_pos > y_upper) && (can_scroll_down && can_scroll_up))
		{
			level->chapterNames[i]->attributes.visibility = false;
		}
		else if (y_pos > y_lower && y_pos < y_upper)
		{
			level->chapterNames[i]->attributes.visibility = true;
		}

		if (y_pos != -1.0f)
		{
			level->chapterNames[i]->spriteComponent()->getSprite()->yPos(y_pos);
		}
	}

	for (int i = 0; i < level->num_of_levels; i++)
	{
		if (y_offset > 0 && can_scroll_down)
		{
			y_pos = level->level_storage[i].tile->spriteComponent()->getSprite()->yPos() + (speed * dt_sec);
		}
		else if (y_offset < 0 && can_scroll_up)
		{
			y_pos = level->level_storage[i].tile->spriteComponent()->getSprite()->yPos() - (speed * dt_sec);
		}

		if ((y_pos < (y_lower + 50) || y_pos > (y_upper - 50)) && (can_scroll_down && can_scroll_up))
		{
			level->level_storage[i].tile->attributes.visibility = false;
		}
		else if (y_pos > (y_lower + 50) && y_pos < (y_upper - 50))
		{
			level->level_storage[i].tile->attributes.visibility = true;
		}

		if (y_pos != -1.0f)
		{
			level->level_storage[i].tile->spriteComponent()->getSprite()->yPos(y_pos);
		}
	}
}

Menu::menus Menu::getCurrentMenu()
{
	return current_menu;
}

void Menu::setCurrentMenu(Menu::menus menu_i)
{
	for (int i = 0; i < num_of_menus; i++)
	{
		for (int j = 0; j <= 32; j++)
		{
			if (menu_storage[i].objects[j].texture_file_location_1.length() < 1) 
			{
				break;
			}
			if (menu_i != i)
			{
				menu_storage[i].objects[j].game_object->attributes.visibility = false;
			}
			else
			{
				if (menu_i == menus::MENU_MAINMENU && menu_storage[i].objects[j].action != "pan")
				{
					menu_storage[i].objects[j].game_object->setOpacity(100);
					menu_storage[i].objects[j].game_object->attributes.visibility = true;
				}
				else
				{
					menu_storage[i].objects[j].game_object->attributes.visibility = true;
				}

				if (menu_storage[i].objects[j].action == "sound-on" && scene->is_sound_muted)
				{
					menu_storage[i].objects[j].game_object->attributes.visibility = false;
				}
				else if (menu_storage[i].objects[j].action == "sound-mute" && !scene->is_sound_muted)
				{
					menu_storage[i].objects[j].game_object->attributes.visibility = false;
				}
			}
		}
	}

	current_menu = menu_i;
	//scene->resetPanning();
}

bool Menu::hasLoaded()
{
	return loaded;
}