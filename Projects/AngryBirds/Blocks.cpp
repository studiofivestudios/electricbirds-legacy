#pragma once

#include "Blocks.h"

Blocks::Blocks()
{
}


Blocks::~Blocks()
{
}

void Blocks::generateBlocks()
{
	block_formation[0].x_pos = 0;
	block_formation[0].y_pos = 0;
	block_formation[0].texture_path = "Resources\\Textures\\puzzlepack\\png\\element_red_rectangle_glossy.png";
	block_formation[0].colour = ASGE::COLOURS::GREEN;

	block_formation[1].x_pos = 64;
	block_formation[1].y_pos = 0;
	block_formation[1].texture_path = "Resources\\Textures\\puzzlepack\\png\\element_red_rectangle_glossy.png";
	block_formation[1].colour = ASGE::COLOURS::BLUE;

	block_formation[2].x_pos = 128;
	block_formation[2].y_pos = 0;
	block_formation[2].texture_path = "Resources\\Textures\\puzzlepack\\png\\element_red_rectangle_glossy.png";
	block_formation[2].colour = ASGE::COLOURS::NAVAJOWHITE;

	block_formation[3].x_pos = 192;
	block_formation[3].y_pos = 0;
	block_formation[3].texture_path = "Resources\\Textures\\puzzlepack\\png\\element_red_rectangle_glossy.png";
	block_formation[3].colour = ASGE::COLOURS::ORANGE;

	block_formation[3].x_pos = 256;
	block_formation[3].y_pos = 0;
	block_formation[3].texture_path = "Resources\\Textures\\puzzlepack\\png\\element_red_rectangle_glossy.png";
	block_formation[3].colour = ASGE::COLOURS::PALEVIOLETRED;

	block_formation[4].x_pos = 320;
	block_formation[4].y_pos = 0;
	block_formation[4].texture_path = "Resources\\Textures\\puzzlepack\\png\\element_red_rectangle_glossy.png";
	block_formation[4].colour = ASGE::COLOURS::ORANGE;

	block_formation[5].x_pos = 384;
	block_formation[5].y_pos = 0;
	block_formation[5].texture_path = "Resources\\Textures\\puzzlepack\\png\\element_red_rectangle_glossy.png";
	block_formation[5].colour = ASGE::COLOURS::NAVAJOWHITE;

	block_formation[6].x_pos = 448;
	block_formation[6].y_pos = 0;
	block_formation[6].texture_path = "Resources\\Textures\\puzzlepack\\png\\element_red_rectangle_glossy.png";
	block_formation[6].colour = ASGE::COLOURS::BLUE;

	block_formation[7].x_pos = 512;
	block_formation[7].y_pos = 0;
	block_formation[7].texture_path = "Resources\\Textures\\puzzlepack\\png\\element_red_rectangle_glossy.png";
	block_formation[7].colour = ASGE::COLOURS::GREEN;
}

